#!/usr/bin/env python
#
# Test components of the survey module.
#
import pyicex.survey as S
import numpy as np


SQRT_2 = 2.**0.5
HYD_RANGES = np.array([[0., 20., 20.*SQRT_2, 20.],
                       [20., 0., 20., 20.*SQRT_2],
                       [20.*SQRT_2, 20., 0., 20.],
                       [20., 20.*SQRT_2, 20., 0.]])
HYD_XY = np.array([[-20., 0.],
                   [-20., -20.],
                   [0, -20],
                   [0., 0.]])
TARGET_RANGES = np.array([10.*SQRT_2, 10.*SQRT_2, 10.*SQRT_2, 10.*SQRT_2])
TARGET_POS = np.array([-10., -10., 0., -135.])


def test_xyfit():
    xy = S.xyfit(HYD_RANGES)
    np.testing.assert_array_almost_equal(xy, HYD_XY)


def test_spherical():
    triplets = [(0, 1, 2),
                (0, 1, 3),
                (1, 2, 3)]
    for t in triplets:
        result = S.trilateration(HYD_XY[t,], TARGET_RANGES[list(t)])
        np.testing.assert_array_almost_equal(np.array(result), TARGET_POS,
                                             err_msg='Failed at {0}'.format(str(t)))
