#!/usr/bin/env python
#
# Test components of the tracking module.
#
import pyicex.tracking as trk
from decimal import Decimal

# Input data building blocks
TIMESTAMP = ['22/10/2014', '21:42:00']
FREQ1 = ['3.2', '-5.6', '423', '0.01293', '0.01567', '0.01587']
FREQ2 = ['40.0', '-27.2', '234', '0.03047', '0.02620', '0.03117']
NORESULT = ['NaN', 'NaN', '999', 'NaN', 'NaN', 'NaN']


def test_freq1():
    data = TIMESTAMP + FREQ1 + NORESULT
    line = '\t'.join(data)
    f1, f2 = trk.parse_txy(line)
    assert f2 is None
    assert f1.x == Decimal(FREQ1[0])
    assert f1.y == Decimal(FREQ1[1])
    assert f1.tt[3] == Decimal(FREQ1[3])
    assert f1.tt[0] == Decimal('-1')


def test_freq2():
    data = TIMESTAMP + NORESULT + FREQ2
    line = '\t'.join(data)
    f1, f2 = trk.parse_txy(line)
    assert f1 is None
    assert f2.x == Decimal(FREQ2[0])
    assert f2.y == Decimal(FREQ2[1])
    assert f2.tt[1] == Decimal(FREQ2[3])
    assert f2.tt[0] == Decimal('-1')
