#!/usr/bin/env python
#
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
from gevent.queue import Queue, Empty
import _socket
sys.path.insert(0, '../pyicex')
from serialcom import Port
from rfgps import RfGps, datarecord_to_list, GpsError
from support import start_client, start_server

SAMPLE_1 = b'001,091012,151400,47.123456,-122.567890,A,12.34,0.211,17.2'
SAMPLE_3 = b'011,091012,151400,47.234567,-122.678901,A,12.34,0.200,16.2'
ERROR = b'010,-1,"INVALID CMD"'

def rfgps_sim(ev, socket, addr):
    """
    Simulate an RF-GPS unit.
    """
    try:
        while True:
            frame = socket.recv(7)
            if frame.startswith('$S001'):
                socket.send('$'+SAMPLE_1+'\r\n')
            elif frame.startswith('$S010'):
                socket.send('$'+ERROR+'\r\n')
            elif frame.startswith('$S011'):
                socket.send('$'+SAMPLE_3+'\r\n')
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()


class RfGpsTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, rfgps_sim)
        self.sock = start_client(self.pathname)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log and hasattr(self.log, 'getvalue'):
            print self.log.getvalue()

    def test_9_timeout(self):
        """Test timeout handling"""
        bh = open('/dev/null', 'r+')
        self.port.serial.fileno.return_value = bh.fileno()
        self.port.open()
        gps = RfGps(self.port, 1)
        d = gps.sample(timeout=2)
        self.assertIsNone(d, 'Timeout not detected (%r)' % d)

    def test_1_sample(self):
        """Test GPS sampling"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        gps = RfGps(self.port, 1)
        d = gps.sample(timeout=3)
        sample = ','.join(datarecord_to_list(d))
        self.assertEqual(sample, SAMPLE_1, 'Bad sample: %r' % repr(sample))

    def test_2_error(self):
        """Test error detection"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        gps = RfGps(self.port, 2)
        self.assertRaises(GpsError, gps.sample, timeout=3)
        
    def test_3_multi(self):
        """Test talking to multiple units"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        gps_1 = RfGps(self.port, 1)
        gps_3 = RfGps(self.port, 3)
        self.assertEqual(gps_1.reader, gps_3.reader)
        d_1 = gps_1.sample(timeout=3)
        d_3 = gps_3.sample(timeout=3)
        sample_1 = ','.join(datarecord_to_list(d_1))
        self.assertEqual(sample_1, SAMPLE_1, 'Bad sample: %r' % repr(sample_1))
        sample_3 = ','.join(datarecord_to_list(d_3))
        self.assertEqual(sample_3, SAMPLE_3, 'Bad sample: %r' % repr(sample_3))
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(RfGpsTest)
    unittest.TextTestRunner(verbosity=2).run(suite)