#!/usr/bin/env python
#
"""
%prog [options] name,x,y [name,x,y ...]

Watch one or more X-Y coordinates and write the corresponding lat-lon
positions to standard output whenever the grid location is updated.
"""
import redis
import sys
import subprocess
import signal
import simplejson as json
from decimal import Decimal
from optparse import OptionParser
from contextlib import contextmanager
from pyicex.geo import Grid, Point, GridPoint, great_circle


@contextmanager
def ignore_signal(signum):
    handler = signal.signal(signum, signal.SIG_IGN)
    try:
        yield
    finally:
        signal.signal(signum, handler)


def watch(rd, points):
    """
    Generator to listen for grid updates published on a Redis channel and use
    this information to convert a series of X-Y points to lat-lon.

    :param rd: Redis server interface
    :type rd: redis.StrictRedis
    :param points: list of name,x,y tuples
    """
    pubsub = rd.pubsub()
    pubsub.subscribe('grid.update')
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            data = json.loads(msg['data'], parse_float=Decimal)
            grid = Grid(Point(lat=data['lat'], lon=data['lon']),
                        y_azimuth=data['azimuth'],
                        units='us-yd')
            for name, x, y in points:
                p = grid.tolatlon(GridPoint(x=x, y=y))
                yield data['timestamp'], name, p.lat, p.lon


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(host='localhost', port=6379,
                        database=0, garmin=None)
    parser.add_option('--host',
                      type='string',
                      dest='host',
                      metavar='HOSTNAME',
                      help='Redis hostname (default: %default)')
    parser.add_option('-p', '--port',
                      type='int',
                      dest='port',
                      help='Redis TCP port (default: %default)')
    parser.add_option('-d', '--database',
                      type='int',
                      dest='database',
                      help='Redis database number (default: %default)')
    parser.add_option('-g', '--garmin',
                      type='string',
                      dest='garmin',
                      help='Garmin serial device for GPS download')
    opts, args = parser.parse_args()

    if len(args) == 0:
        parser.error('You must specify at least one grid-point')

    rd = redis.StrictRedis(host=opts.host,
                           port=opts.port,
                           db=opts.database)
    points = []
    for arg in args:
        name, x, y = arg.split(',')
        points.append((name, float(x), float(y)))

    if opts.garmin:
        gpsbabel = 'gpsbabel -w -i unicsv,grid=ddd -f - -o garmin -F {0}'.format(opts.garmin)
    else:
        gpsbabel = None

    t0, lat0, lon0 = 0, 0, 0
    try:
        for t, name, lat, lon in watch(rd, points):
            if t0 > 0:
                dist, fwd, back = great_circle(Point(lat=lat0, lon=lon0),
                                               Point(lat=lat, lon=lon))
                v = dist / (t - t0)
                sys.stdout.write('V:{0},{1:.2f},{2:.1f}\n'.format(name, v, fwd))
            t0, lat0, lon0 = t, lat, lon
            hlat = lat < 0 and 'S' or 'N'
            hlon = lon < 0 and 'W' or 'E'
            if opts.garmin:
                try:
                    p = subprocess.Popen(gpsbabel.split(),
                                         stdin=subprocess.PIPE)
                    with ignore_signal(signal.SIGINT):
                        pipe = p.stdin
                        pipe.write('Name,Latitude,Longitude,Alt\n')
                        pipe.write('{0},{1}{2:.6f},{3}{4:.6f},0\n'.format(
                                   name, hlat, abs(lat), hlon, abs(lon)))
                        pipe.close()
                    p.wait()
                    sys.stderr.write('Waypoint {0} transferred\n'.format(name))
                except Exception as e:
                    sys.stderr.write('Cannot run gpsbabel ({0})\n'.format(str(e)))
            else:
                sys.stdout.write('P:{0},{1},{2}{3:.6f},{4}{5:.6f}\n'.format(
                    t, name, hlat, abs(lat), hlon, abs(lon)
                ))
    except KeyboardInterrupt:
        sys.stderr.write('Exiting\n')

if __name__ == '__main__':
    main()
