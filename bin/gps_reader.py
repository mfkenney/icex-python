#!/usr/bin/env python
#
"""
Sample the ICEX camp and remote GPS units. Publish the data records
on the following Redis pub/sub channels:

  - data.gps
  - data.rfgps
  - data.grid
  - data.ranges

"""
from gevent import monkey
monkey.patch_all()
import sys
import logging
import yaml
import redis
import time
import argparse
import random
import simplejson as json
from ast import literal_eval
from decimal import Decimal
from datetime import datetime
import pytz
from pyicex.serialcom import Port
from pyicex.rfgps import RfGps, GpsError, RfgpsData
from pyicex.gps import GarminGps
from pyicex import unix_time, DataRecordEncoder
from pyicex.geo import Point, GridPoint, Grid
from pyicex.survey import ranges


CONFIG = """
mq:
  host: localhost
  port: 6379
  db: 0
gps:
  device: /dev/ttyS1
  baud: 4800
rfgps:
  device: /dev/ttyS2
  baud: 19200
  addresses: [0, 1, 2, 3]
params:
  rfgps_check: 10
"""


def deep_update(d1, d2):
    """
    Update *d1* with the contents of *d2* recursively updating
    any subdictionaries.

    >>> d1 = {'a': 1, 'b': {'a1': 2, 'b1': 42}}
    >>> d2 = {'c': 'foo', 'b': {'c1': 'baz', 'a1': 5}}
    >>> deep_update(d1, d2)
    >>> d1 == {'a': 1, 'b': {'a1': 5, 'b1': 42, 'c1': 'baz'}, 'c': 'foo'}
    True
    """
    for k, v in d2.iteritems():
        if isinstance(v, dict):
            if k in d1:
                deep_update(d1[k], v)
            else:
                d1[k] = v
        else:
            d1[k] = v


def rfgps_sim(dev, gpsdata, rms=1.5):
    coords = [(-100, 100), (-100, -100), (100, -100), (100, 100)]
    g = Grid(Point(lat=gpsdata.lat, lon=gpsdata.lon), 0.0)
    xy = coords[dev.address]
    p = g.tolatlon(GridPoint(x=xy[0]+random.gauss(0., rms),
                             y=xy[1]+random.gauss(0., rms)))
    return RfgpsData(address=dev.address,
                     timestamp=datetime.utcnow().replace(tzinfo=pytz.utc),
                     lat=p.lat,
                     lon=p.lon,
                     status='A',
                     vbatt=Decimal('12.34'),
                     ibatt=Decimal('0.789'),
                     temp=Decimal('1.23'))


def mainloop(gps, rfgps, rd, params, fake_rf=False):
    """
    Main sampling loop for GPS data.

    :param gps:
    :type gps: GarminGps
    :param rfgps: list of RfGps objects
    :param rd: Redis server interface
    :type rd: redis.StrictRedis
    :param params: parameters from the configuration file
    """
    def _sample(dev):
        try:
            rec = dev.sample(timeout=3)
        except GpsError:
            rec = None
        return rec
    last_interval = 0
    while True:
        try:
            data = gps.read(timeout=3)
            logging.debug('GPS> %r', data)
        except IOError:
            text = 'No data from Origin GPS'
            alert = dict(
                timestamp=int(time.time()),
                text=text)
            rd.publish('grid.alert', json.dumps(alert))
            logging.critical(text)
            continue
        value = json.dumps(data, cls=DataRecordEncoder)
        rd.publish('data.gps', value)
        rd.set('data.gps:current', value)
        t = unix_time(data.timestamp)
        try:
            interval, _ = divmod(t, params['rfgps_check'])
        except ZeroDivisionError:
            interval = 0
        points = []
        if interval > last_interval:
            logging.debug('Polling RF-GPS')
            last_interval = interval
            for dev in rfgps:
                rdata = rfgps_sim(dev, data) if fake_rf else _sample(dev)
                if rdata is not None:
                    logging.debug('RFGPS> %r', rdata)
                    rd.publish('data.rfgps', json.dumps(rdata,
                               cls=DataRecordEncoder))
                    points.append({'lat': rdata.lat,
                                   'lon': rdata.lon,
                                   'address': rdata.address})
                    if rdata.status != 'A':
                        text = 'Bad GPS fix from unit {0}'.format(
                            dev.address)
                        alert = dict(
                            timestamp=int(time.time()),
                            text=text)
                        logging.critical(text)
                        rd.publish('grid.alert', json.dumps(alert))
                else:
                    text = 'No data from unit {0}'.format(dev.address)
                    alert = dict(
                        timestamp=int(time.time()),
                        text=text)
                    logging.critical(text)
                    rd.publish('grid.alert', json.dumps(alert))

            rd.set('rfgps:current', json.dumps({'points': points}))
            rd.publish('data.grid',
                       json.dumps({'t': data.timestamp,
                                   'origin': {
                                       'lat': data.lat,
                                       'lon': data.lon,
                                       'status': data.status
                                   },
                                   'points': points},
                                  cls=DataRecordEncoder))

            logging.debug('Generating survey ranges')
            # Before generating the range matrix, we need to sort
            # the list of points into hydrophone order. Use the
            # rfpgs:map data store entry to map RF-GPS addresses
            # to hydrophone names. If the map does not exist, the
            # points are assumed to be in hydrophone order.
            hydros = rd.hgetall('rfgps:map') or {}
            for i in range(len(points)):
                p = points[i]
                name = hydros.get(str(p['address']), 'H' + str(p['address']+1))
                points[i] = (name, p)
            points.sort()
            rpoints = []
            addrs = []
            rpoints.append(Point(lat=data.lat, lon=data.lon))
            for i, p in enumerate(points):
                rpoints.append(Point(lat=p[1]['lat'], lon=p[1]['lon']))
                addrs.append(p[1]['address'])
            R, az = ranges(rpoints)
            rd.publish('data.ranges', json.dumps({'R': R.tolist(),
                                                  'timestamp': t,
                                                  'addrs': addrs,
                                                  'az': az.tolist()}))


def main():
    """
    Sample the ICEX camp and remote GPS units.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('cfgfile', help='configuration file', nargs='?',
                        type=argparse.FileType('r'))
    parser.add_argument('-o', '--option',
                        help='override a configuration option',
                        default=[],
                        action='append', metavar='KEY=VALUE')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--fake-rf',
                        action='store_true',
                        help='generate fake RF-GPS data for testing')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    # Load the default configuration and then override with..
    #
    # 1. the optional command-line configuration file
    # 2. the optional command-line arguments
    #
    cfg = yaml.load(CONFIG)
    if args.cfgfile:
        try:
            cfgfile = yaml.safe_load(args.cfgfile)
        except yaml.YAMLError:
            logging.exception('Error in config file')
            sys.exit(1)
        else:
            deep_update(cfg, cfgfile)
    # Each option has the form KEY=VALUE. The KEYs are hierarchical
    # with each level separated by a '.'. For example:
    #
    #     -o gps.device=/dev/ttyS0
    #
    for opt in args.option:
        try:
            key, value = opt.split('=')
            parts = key.split('.')
            k = parts[-1]
            d = cfg
            for p in parts[:-1]:
                if isinstance(d[p], dict):
                    d = d[p]
                else:
                    break
            try:
                d[k] = literal_eval(value)
            except (ValueError, SyntaxError):
                d[k] = value
        except KeyError:
            logging.warning('Bad option: {0}\n'.format(opt))

    rd = redis.StrictRedis(host=cfg['mq']['host'],
                           port=cfg['mq'].get('port', 6379),
                           db=cfg['mq'].get('db', 0))

    # Open GPS device
    port = Port(cfg['gps']['device'], baud=cfg['gps']['baud'])
    port.open()
    logging.info('Open port %r', port)
    gps = GarminGps(port)

    # Open RF-GPS devices
    if cfg['params']['rfgps_check'] > 0:
        port = Port(cfg['rfgps']['device'], baud=cfg['rfgps']['baud'])
        port.open()
        logging.info('Open port %r', port)
        rfgps = [RfGps(port, addr) for addr in cfg['rfgps']['addresses']]
    else:
        rfgps = []

    gps.start()
    try:
        mainloop(gps, rfgps, rd, cfg['params'], fake_rf=args.fake_rf)
    except KeyboardInterrupt:
        logging.info('Exiting ...')
        gps.stop()


if __name__ == '__main__':
    main()
