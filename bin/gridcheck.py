#!/usr/bin/env python
#
"""
Monitor the Redis data.grid channel and use the information to
perform regular checks of the ICEX grid orientation.
"""
import sys
import yaml
import math
import redis
import argparse
import logging
import simplejson as json
from ast import literal_eval
from decimal import Decimal
from collections import namedtuple
from pyicex.geo import Point, great_circle
from pyicex import EMA


CONFIG = """
mq:
  host: localhost
  port: 6379
  db: 0
dbkey: survey
units: us-yd
watch: 2
alpha: 0.1
"""

GridData = namedtuple('GridData', 'timestamp lat lon azimuth refs')
RefPoint = namedtuple('RefPoint', 'x y range bearing')

# Scale factors to convert from various grid units to meters
GRID_SCALES = {
    'm': 1.,
    'km': 1000.,
    'nmi': 1852.,
    'yd': 0.9144,
    'yds': 0.9144,
    'us-yd': 0.914401828803658,
    'us-yds': 0.914401828803658
}


def deep_update(d1, d2):
    """
    Update *d1* with the contents of *d2* recursively updating
    any subdictionaries.

    >>> d1 = {'a': 1, 'b': {'a1': 2, 'b1': 42}}
    >>> d2 = {'c': 'foo', 'b': {'c1': 'baz', 'a1': 5}}
    >>> deep_update(d1, d2)
    >>> d1 == {'a': 1, 'b': {'a1': 5, 'b1': 42, 'c1': 'baz'}, 'c': 'foo'}
    True
    """
    for k, v in d2.iteritems():
        if isinstance(v, dict):
            if k in d1:
                deep_update(d1[k], v)
            else:
                d1[k] = v
        else:
            d1[k] = v


def median(x):
    n = len(x)
    x.sort()
    if (n & 1) == 1:
        return x[n // 2]
    else:
        j = n // 2
        i = j - 1
        return (x[i] + x[j]) / 2.


def get_ref_points(rd, dbkey, tstamp):
    """
    Return the most recent set of survey reference points from
    the Redis data store if it is newer than the specified
    time-stamp.
    """
    refs = {}
    try:
        survey = json.loads(rd.lindex(dbkey, -1))
    except TypeError:
        logging.debug('Survey lookup failed')
        return 0, refs

    if survey['timestamp'] > tstamp:
        logging.info('New survey available')
        for p in survey['points']:
            x = p['x']
            y = p['y']
            # Calculate the range and bearing to each ref point
            refs[p['address']] = RefPoint(x=x, y=y,
                                          range=math.sqrt(x**2 + y**2),
                                          bearing=math.degrees(
                                              math.pi / 2. - math.atan2(y, x)))
    return survey['timestamp'], refs


def process(rd, units, filter_coeff, max_rng_error,
            chan='data.grid', refkey='survey'):
    """
    Read grid location updates from a pub/sub channel and process them.

    :param rd: Redis server interface
    :type rd: redis.StrictRedis
    :param units: grid coordinate units
    :param filter_coeff: coefficient for EMA range filter
    :param max_rng_error: maximum allowed range error
    :param chan: channel name
    :param refkey: data-store key for survey reference points
    """
    # Time of last survey
    t_survey = 0
    # Survey reference points
    refpoints = {}
    # Moving-average filters for reference point ranges
    filters = {}
    # Scale factor to convert meters to grid units
    scale = 1. / GRID_SCALES.get(units, 1.)

    pubsub = rd.pubsub()
    pubsub.subscribe(chan)

    for msg in pubsub.listen():
        if msg['type'] != 'message':
            continue
        logging.debug('> %r', msg['data'])
        try:
            rec = json.loads(msg['data'], parse_float=Decimal)
        except Exception:
            logging.exception('Message decode error')
            continue

        # Check if the survey has been updated
        t_survey, rp = get_ref_points(rd, refkey, t_survey)
        if rp:
            logging.info('Updating reference points')
            refpoints = rp
            if filters:
                for k in filters.keys():
                    filters[k].reset()
            else:
                filters = dict([(key, EMA(filter_coeff))
                                for key in refpoints.keys()])
            # Initialize filters with the surveyed ranges
            for k in filters.keys():
                filters[k](refpoints[k].range)

        # We must short-circuit the loop unless a survey
        # has been done.
        if not refpoints:
            logging.debug('No survey available')
            continue

        p0 = Point(lat=rec['origin']['lat'],
                   lon=rec['origin']['lon'])
        dtheta = []
        refs = []
        for p in rec['points']:
            ref = refpoints.get(p['address'])
            if ref:
                key = p['address']
                name = 'RF-GPS-{0}'.format(key)
                p1 = Point(lat=p['lat'], lon=p['lon'])
                dist, fwd, back = great_circle(p0, p1)
                # Distance returned by great_circle is in meters, we need to
                # convert this to the grid coordinate units.
                r = filters[key](dist * scale)
                dr = r - ref.range
                daz = fwd - ref.bearing
                # Remap bearing change to [-180, 180]
                if daz > 180.:
                    daz = daz - 360.
                elif daz < -180.:
                    daz = daz + 360.

                refs.append(dict(name=name,
                                 range=Decimal('{0:.2f}'.format(dr)),
                                 bearing=Decimal('{0:.2f}'.format(daz))))
                if abs(dr) > max_rng_error:
                    text = '{0} outside watch circle ({1:.1f})'.format(
                        name, dr)
                    alert = json.dumps({
                        'timestamp': rec['t'],
                        'text': text
                    })
                    logging.critical(text)
                    rd.publish('grid.alert', alert)
                    # Reset the averaging filter for this unit
                    filters[key].reset()
                else:
                    # Store the change in bearing
                    dtheta.append(daz)
        if dtheta:
            azimuth = median(dtheta) % 360
        result = GridData(lat=rec['origin']['lat'],
                          lon=rec['origin']['lon'],
                          timestamp=rec['t'],
                          azimuth=Decimal('{0:.2f}'.format(azimuth)) % 360,
                          refs=refs)
        # Encode as JSON, broadcast to subscribers and store in database entry
        value = json.dumps(result)
        rd.publish('grid.update', value)
        rd.set('grid.update:current', value)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('cfgfile', help='configuration file', nargs='?',
                        type=argparse.FileType('r'))
    parser.add_argument('-o', '--option',
                        help='override a configuration option',
                        default=[],
                        action='append', metavar='KEY=VALUE')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    # Load the default configuration and then override with..
    #
    # 1. the optional command-line configuration file
    # 2. the optional command-line arguments
    #
    cfg = yaml.load(CONFIG)
    if args.cfgfile:
        try:
            cfgfile = yaml.safe_load(args.cfgfile)
        except yaml.YAMLError:
            logging.exception('Error in config file')
            sys.exit(1)
        else:
            deep_update(cfg, cfgfile)
    # Each option has the form KEY=VALUE. The KEYs are hierarchical
    # with each level separated by a '.'. For example:
    #
    #     -o gps.device=/dev/ttyS0
    #
    for opt in args.option:
        try:
            key, value = opt.split('=')
            parts = key.split('.')
            k = parts[-1]
            d = cfg
            for p in parts[:-1]:
                if isinstance(d[p], dict):
                    d = d[p]
                else:
                    break
            try:
                d[k] = literal_eval(value)
            except (ValueError, SyntaxError):
                d[k] = value
        except KeyError:
            logging.warning('Bad option: {0}\n'.format(opt))

    rd = redis.StrictRedis(host=cfg['mq']['host'],
                           port=cfg['mq'].get('port', 6379),
                           db=cfg['mq'].get('db', 0))

    try:
        process(rd, cfg['units'], cfg['alpha'], cfg['watch'],
                refkey=cfg.get('dbkey', 'survey'))
    except KeyboardInterrupt:
        logging.info('Exiting ...')


if __name__ == '__main__':
    main()
