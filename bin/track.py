#!/usr/bin/env python
#
#
from __future__ import absolute_import, division, print_function
import argparse as AP
import numpy as np
from pyicex import unix_time
from pyicex.survey import trilateration
from pyicex.tracking import parse_ts, read_survey


TIMEFMT = '%Y-%m-%d %H:%M:%S'


def solve(hyds, ranges):
    """
    Return X-Y position of a target, given the hydrophone locations
    and target ranges.
    """
    triplets = [
        [0, 1, 2],
        [0, 1, 3],
        [1, 2, 3]
    ]

    solutions = []
    for t in triplets:
        x, y, z, _ = trilateration(hyds[t], ranges[t])
        solutions.append((abs(z), x, y, t))
    solutions.sort()
    if len(solutions):
        return solutions[0][1:]
    else:
        return None


def main():
    """
    Convert timestamps to X-Y locations.
    """
    parser = AP.ArgumentParser(description=main.__doc__)
    parser.add_argument('surveyfile',
                        type=file,
                        help='file of hydrophone locations')
    parser.add_argument('tsfile',
                        type=file,
                        help='timestamp file')
    parser.add_argument('-s', '--sound-speed',
                        type=float,
                        dest="sound_speed",
                        default=1640,
                        help='sound speed in yards/s')
    args = parser.parse_args()

    hyds = np.array(read_survey(args.surveyfile))
    for line in args.tsfile:
        for rec in parse_ts(line, wantall=False):
            if rec.freq == 1 and rec.quad == 1:
                _, sec = divmod(unix_time(rec.timestamp), 10)
                r = (np.array([float(x) for x in rec.tt])
                     + sec) * args.sound_speed
                result = solve(hyds, r)
                if result is not None:
                    x, y, hs = result
                    print(rec.timestamp.strftime(TIMEFMT),
                          round(x, 1),
                          round(y, 1),
                          ''.join([str(h+1) for h in hs]),
                          sep=',')


if __name__ == '__main__':
    main()
