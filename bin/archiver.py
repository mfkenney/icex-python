#!/usr/bin/env python
#
"""
Usage: archiver.py cfgfile

Manage the GPS and Met-station data archive for ICEX
"""
import sys
import yaml
import redis
import os
import time
import logging
import signal
import argparse
import simplejson as json
from urlparse import urlparse
from threading import Thread, Event
from influxdb import InfluxDBClient
from pyicex import tocsv


def signal_handler(signum, frame):
    sys.exit(0)


class Archive(object):
    """
    Class to manage a single CSV data archive.

    :ivar ardir: archive directory path
    :ivar t_rollover: file roll-over interval in seconds
    :ivar ftmpl: filename template in :func:`time.strftime` format
    :ivar header: sequence of file header strings
    """
    eol = '\n'

    def __init__(self, ardir, ftmpl, header, t_rollover=86400, logger=None):
        """
        Instance initializer.

        :param ardir:
        :param ftmpl:
        :param t_rollover:
        :param header:
        :return:
        """
        self.ardir = ardir
        self.ftmpl = ftmpl
        self.t_rollover = t_rollover
        if not header:
            raise ValueError('Missing header parameter')
        self.header = header
        self.timestamp = 0, 0
        self._file = None
        if not os.path.isdir(self.ardir):
            os.makedirs(self.ardir)
        self.logger = logger or logging.getLogger()

    def _openfile(self):
        """
        Open the current archive file.

        :return: file object
        """
        t = divmod(int(time.time()), self.t_rollover)
        if (self._file is None) or (t[0] > self.timestamp[0]):
            fname = time.strftime(self.ftmpl, time.gmtime())
            if self._file:
                self._file.close()
            pathname = os.path.join(self.ardir, fname)
            # If file exists, open in append mode. Otherwise open for writing
            # and add the header.
            if os.path.isfile(pathname):
                self._file = open(pathname, 'a')
            else:
                self._file = open(pathname, 'w')
                self._file.write(','.join(self.header) + self.eol)
            self.logger.info('Opened archive file %s', pathname)
            self.timestamp = t
        return self._file

    def _closefile(self):
        if self._file:
            self._file.close()
            self._file = None

    def __call__(self, ev, rd, chan, interval=0, dbclient=None,
                 tablename=None, tagvars=None):
        """
        Task to monitor a Redis pubsub channel for JSON-encoded data
        messages. The contents of each message are converted to CSV
        and appended to the data file.

        :param ev: stop event
        :param rd: Redis server instance
        :param chan: channel name
        :param name: storage interval in seconds (0 means store every
                     data record)
        :param dbclient: timestamp-database client
        :param tablename: database measurement name
        :param tagvars: record variable names which will be stored as
                        tags in the database.
        """
        self.logger.info('Starting')
        dbrec = {}
        dbrec['measurement'] = tablename or chan
        dbrec['fields'] = {}
        tags = tagvars or []
        # The first word of each header element is the data
        # value key.
        keys = []
        for h in self.header:
            key = h.split()[0]
            if key == 'time':
                key = 'timestamp'
            keys.append(key)
        pubsub = rd.pubsub()
        pubsub.subscribe(chan)
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                self.logger.debug('< %r', msg['data'])
                try:
                    d = json.loads(msg['data'])
                    record = tocsv([d[k] for k in keys])
                    if dbclient:
                        dbrec['fields'] = dict([(k, d[k]) for k in keys])
                        dbrec['time'] = int(dbrec['fields'].pop(
                            'timestamp') * 1e9)
                        # Move any variables which will be treated as tags
                        # from the 'fields' dict to the 'tags' dict.
                        dbrec['tags'] = {}
                        for t in tags:
                            dbrec['tags'][t] = str(dbrec['fields'].pop(t))
                        dbclient.write_points([dbrec])
                except Exception:
                    self.logger.exception('Record decode error')
                else:
                    if interval == 0 or (d['timestamp'] % interval) == 0:
                        ofp = self._openfile()
                        ofp.write(record + self.eol)
                        ofp.flush()
                        rd.hincrby('archiver', chan, 1)
            if ev.is_set():
                break

        self._closefile()
        self.logger.info('Exiting')


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('cfgfile', help='configuration file',
                        type=argparse.FileType('r'))
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger('archiver')
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(
        logging.Formatter('%(levelname)-8s %(threadName)-10s %(message)s'))
    logger.addHandler(ch)

    try:
        cfg = yaml.safe_load(args.cfgfile)
    except IOError:
        logger.exception('Cannot open config file')
        sys.exit(1)
    except yaml.YAMLError:
        logger.exception('Error in config file')
        sys.exit(1)

    rd = redis.StrictRedis(host=cfg['mq'].get('host', 'localhost'),
                           port=cfg['mq'].get('port', 6379),
                           db=cfg['mq'].get('db', 0))
    # Store our PID in Redis so other processes can check that
    # we are running.
    rd.set('archiver.pid', str(os.getpid()))
    signal.signal(signal.SIGTERM, signal_handler)

    try:
        tasks = []
        ev = Event()
        for desc in cfg['archives']:
            archive = Archive(os.path.expanduser(desc['dir']),
                              desc['template'],
                              desc['header'],
                              t_rollover=desc['rollover'],
                              logger=logger)
            # Are we using the InfluxDB time-series database?
            tags = []
            if 'db' in desc:
                dbname = desc['db']['name']
                tablename = desc['db']['measurement']
                url = '{}/{}'.format(desc['db']['url'], dbname)
                client = InfluxDBClient.from_DSN(url)
                client.create_database(dbname, if_not_exists=True)
                tags = desc['db'].get('tags', [])
            else:
                tablename, client = None, None

            task = Thread(target=archive,
                          name=desc['channel'],
                          args=(ev, rd, desc['channel']),
                          kwargs={'interval': desc.get('interval', 0),
                                  'dbclient': client,
                                  'tablename': tablename,
                                  'tagvars': tags})
            rd.hset('archiver', desc['channel'], 0)
            task.daemon = True
            task.start()
            tasks.append(task)

        signal.pause()
    except (KeyboardInterrupt, SystemExit):
        logger.info('Exiting ...')
    except Exception:
        logger.exception('Unhandled exception')
    finally:
        ev.set()
        for task in tasks:
            task.join(3)
            if task.is_alive():
                logger.info('Thread %s did not halt', task.name)
        rd.delete('archiver.pid')

if __name__ == '__main__':
    main()
