#!/usr/bin/env python
#
import gevent
from gevent.queue import JoinableQueue
from gevent import monkey
monkey.patch_all()
import redis


def save_history(dataq, key='weather.history', maxlen=120):
    rd = redis.StrictRedis()
    while True:
        rec = dataq.get()
        rd.rpush(key, rec)
        rd.ltrim(key, -maxlen, -1)
        dataq.task_done()


def main():
    dataq = JoinableQueue()
    task = gevent.spawn(save_history, dataq)
    rd = redis.StrictRedis()
    pubsub = rd.pubsub()
    pubsub.subscribe('data.weather')
    try:
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                dataq.put(msg['data'])
    finally:
        dataq.join()
        task.kill(timeout=5)

if __name__ == '__main__':
    main()
