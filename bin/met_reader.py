#!/usr/bin/env python
#
# Monitor the APLIS Met Station.
#
import sys
import signal
import yaml
import simplejson as json
import redis
import argparse
import logging
from ast import literal_eval
from pyicex.serialcom import Port
from pyicex.met import Wp2000
from pyicex import DataRecordEncoder

CONFIG = """
mq:
  host: localhost
  port: 6379
  db: 0
met:
  device: /dev/ttyUSB0
  baud: 9600
params:
  history: 1440
"""


def signal_handler(signum, frame):
    sys.exit(0)


def deep_update(d1, d2):
    """
    Update *d1* with the contents of *d2* recursively updating
    any subdictionaries.

    >>> d1 = {'a': 1, 'b': {'a1': 2, 'b1': 42}}
    >>> d2 = {'c': 'foo', 'b': {'c1': 'baz', 'a1': 5}}
    >>> deep_update(d1, d2)
    >>> d1 == {'a': 1, 'b': {'a1': 5, 'b1': 42, 'c1': 'baz'}, 'c': 'foo'}
    True
    """
    for k, v in d2.iteritems():
        if isinstance(v, dict):
            if k in d1:
                deep_update(d1[k], v)
            else:
                d1[k] = v
        else:
            d1[k] = v


def main():
    """
    Monitor the APLIS Met Station.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('cfgfile', help='configuration file', nargs='?',
                        type=argparse.FileType('r'))
    parser.add_argument('-o', '--option',
                        help='override a configuration option',
                        default=[],
                        action='append', metavar='KEY=VALUE')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    # Load the default configuration and then override with..
    #
    # 1. the optional command-line configuration file
    # 2. the optional command-line arguments
    #
    cfg = yaml.load(CONFIG)
    if args.cfgfile:
        try:
            cfgfile = yaml.safe_load(args.cfgfile)
        except yaml.YAMLError:
            logging.exception('Error in config file')
            sys.exit(1)
        else:
            deep_update(cfg, cfgfile)
    # Each option has the form KEY=VALUE. The KEYs are hierarchical
    # with each level separated by a '.'. For example:
    #
    #     -o met.device=/dev/ttyS0
    #
    for opt in args.option:
        try:
            key, value = opt.split('=')
            parts = key.split('.')
            k = parts[-1]
            d = cfg
            for p in parts[:-1]:
                if isinstance(d[p], dict):
                    d = d[p]
                else:
                    break
            try:
                d[k] = literal_eval(value)
            except (ValueError, SyntaxError):
                d[k] = value
        except KeyError:
            logging.warning('Bad option: {0}\n'.format(opt))

    rd = redis.StrictRedis(host=cfg['mq']['host'],
                           port=cfg['mq']['port'],
                           db=cfg['mq']['db'])

    history = cfg['params']['history']

    # Open serial device
    port = Port(cfg['met']['device'], baud=cfg['met']['baud'])
    port.open()
    logging.info('Open port %r', port)

    signal.signal(signal.SIGTERM, signal_handler)

    # Read data records and send them to the Redis data-store
    with Wp2000(port) as met:
        try:
            while True:
                rec = met.read()
                logging.debug('> %r', rec)
                msg = json.dumps(rec, cls=DataRecordEncoder)
                rd.publish('data.weather', msg)
                rd.rpush('weather.history', msg)
                rd.ltrim('weather.history', -history, -1)
        except (KeyboardInterrupt, SystemExit):
            logging.info('Exiting ...')

if __name__ == '__main__':
    main()
