#!/usr/bin/env python
#
# Store ICEX GPS and Met data in a RethinkDB database.
#
import redis
import rethinkdb as r
import argparse
import simplejson as json
import sys


def db_setup(conn, db_name, table_names, cache_size=1024*1024):
    if not db_name in r.db_list().run(conn):
        r.db_create(db_name).run(conn)
    db = r.db(db_name)
    tables = db.table_list().run(conn)
    for name in table_names:
        if not name in tables:
            db.table_create(name, cache_size=cache_size).run(conn)
    return db


def store_record(conn, db, name, rec):
    rec['timestamp'] = r.epoch_time(rec['timestamp'])
    db.table(name).insert(rec).run(conn)


def monitor(rd, conn, db, table_map):
    pubsub = rd.pubsub()
    pubsub.psubscribe('data.*')
    for msg in pubsub.listen():
        if msg['type'] == 'pmessage':
            table_name = table_map.get(msg['channel'])
            if table_name:
                store_record(conn, db, table_name,
                             json.loads(msg['data']))


def main():
    """
    Store ICEX GPS and Met data in a RethinkDB database.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('--dbname', help='database name (default: icex2014)',
                        default='icex2014')
    parser.add_argument('--dbhost', help='database host (default: localhost)',
                        default='localhost')
    parser.add_argument('--dbport', help='database port (default: 28015)',
                        default=28015, type=int)
    args = parser.parse_args()

    table_map = {
        'data.weather': 'weather',
        'data.gps': 'gps',
        'data.rfgps': 'rfgps'
    }
    rd = redis.StrictRedis()
    conn = r.connect(host=args.dbhost, port=args.dbport)
    db = db_setup(conn, args.dbname, table_map.values())
    try:
        monitor(rd, conn, db, table_map)
    except KeyboardInterrupt:
        sys.stderr.write('Exiting...\n')

if __name__ == '__main__':
    main()
