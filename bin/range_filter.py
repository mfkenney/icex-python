#!/usr/bin/env python
#
"""
Apply an exponential moving average filter to the data records
published on the *data.ranges* channel. The filtered records are
published to *ranges.filtered*.

"""
import redis
import logging
import argparse
import json
import numpy as np
from pyicex import EMA


def main():
    """
    Filter ICEX grid range data.
    """
    parser = argparse.ArgumentParser(description=main.__doc__,
                                     epilog=__doc__)
    parser.add_argument('alpha',
                        type=float,
                        help='EMA filter alpha parameter')
    parser.add_argument('--host',
                        default='localhost',
                        help='Redis host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=6379,
                        help='Redis TCP port (%(default)d)')
    parser.add_argument('--db',
                        type=int,
                        default=0,
                        help='Redis DB number (%(default)d)')
    parser.add_argument('--dim',
                        type=int,
                        default=5,
                        help='range matrix dimension (%(default)d)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    rd = redis.StrictRedis(host=args.host,
                           port=args.port,
                           db=args.db)
    pubsub = rd.pubsub()
    logging.info('Connected to Redis')

    first = True
    A = np.zeros((args.dim, args.dim))
    pubsub.subscribe('data.ranges')
    logging.info('Listening for messages')
    try:
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                data = json.loads(msg['data'])
                logging.debug('< %r', data)
                R = np.array(data['R'])
                if R.shape == A.shape:
                    if first:
                        A[R.nonzero()] = args.alpha
                        filt = EMA(A)
                        first = False
                    R = filt(R)
                    data['R'] = R.tolist()
                    logging.debug('> %r', data)
                    newmsg = json.dumps(data)
                    rd.set('ranges.filtered:current', newmsg)
                    rd.publish('ranges.filtered', newmsg)
                else:
                    logging.critical('Invalid range matrix size %r',
                                     R.shape)
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    finally:
        pubsub.close()


if __name__ == '__main__':
    main()
