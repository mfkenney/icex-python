#!/usr/bin/env python
#
"""
Convert a GPS track CSV file to KML
"""
from __future__ import print_function
import os
import csv
import time
from datetime import datetime
import pytz
from pykml.factory import KML_ElementMaker as KML
from pykml.factory import GX_ElementMaker as GX
from pykml.factory import nsmap
from zipfile import ZipFile, ZIP_DEFLATED
import lxml
import argparse


# Namespace strings
gxns = '{' + nsmap['gx'] + '}'
defns = '{http://www.opengis.net/kml/2.2}'

icon_url = 'http://earth.google.com/images/kml-icons/track-directional/track-0.png'
mk_url = 'http://maps.google.com/mapfiles/kml/paddle/ylw-circle.png'
marker_text = '''
  <b>Waypoint</b><br/>
  Daily marker: $[timestamp]<br/>
'''


def isotimestamp(t):
    """
    Create an ISO8601 format timestamp from a time in seconds.

    :param t:
    :return:
    """
    if isinstance(t, datetime):
        return t.strftime('%Y-%m-%dT%H:%M:%SZ')
    else:
        return time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(t))


def readcsv(filename):
    """
    Iterate over a CSV file returning a dictionary for each row.

    :param filename: input file name.
    """
    rdr = csv.DictReader(open(filename, 'r'))
    for row in rdr:
        yield row


def create_document(name, desc=None, color='9959acff', width=2):
    """
    Create a new KML document.

    :param name: document name
    :param desc: document description
    :param color: track color as a hex ARGB string
    :param width: track width
    :return: document object
    :rtype: KML.kml
    """
    doc = KML.kml(
        KML.Document(
            KML.name(name),
            KML.Style(
                KML.IconStyle(
                    KML.Icon(
                        KML.href(icon_url)
                    )
                ),
                KML.LineStyle(
                    KML.color(color),
                    KML.width(width)
                ),
                id='track'
            ),
            KML.Style(
                KML.IconStyle(
                    KML.scale(0.6),
                    KML.Icon(
                        KML.href(mk_url)
                    )
                ),
                KML.LabelStyle(
                    KML.scale(0.0)
                ),
                KML.BalloonStyle(
                    KML.text(marker_text)
                ),
                id='markers'
            )
        )
    )
    if desc:
        doc.Document.append(KML.description(desc))
    return doc


def make_placemark(lat, lon, t):
    return KML.Placemark(
        KML.name('Waypoint'),
        KML.ExtendedData(
            KML.Data(
                KML.value(isotimestamp(t)),
                name='timestamp'
            )
        ),
        KML.styleUrl('#markers'),
        KML.Point(
            KML.coordinates('{},{}'.format(lon, lat))),
        KML.TimeStamp(
            KML.when(isotimestamp(t))))


def fill_document(infile, doc, inc=1, title='APLIS'):
    """
    Fill a KML document with GPS track information from a CSV file.

    :param infile: input file name
    :param doc: KML document
    :return: filled document
    """
    trk = GX.Track()
    count = 0
    last_t, dt = None, None
    markers = []
    for row in readcsv(infile):
        if (count % inc) == 0:
            try:
                t = int(row['time'])
                t = datetime.fromtimestamp(t, pytz.utc)
            except ValueError:
                t = datetime.strptime(
                    row['time'],
                    '%Y-%m-%d %H:%M:%S.%f').replace(tzinfo=pytz.utc)
            az = row.get('azimuth', '0')
            lat = row.get('lat', row.get('latitude', 0))
            lon = row.get('lon', row.get('longitude', 0))
            trk.append(
                KML.when(isotimestamp(t))
            )
            if last_t:
                dt = t - last_t
            last_t = t
            sec = t.second + (60 * (t.minute + 60*t.hour))
            # Daily markers
            if dt and (0 < sec <= dt.total_seconds()):
                markers.append(make_placemark(lat, lon, t))
                print(t, lat, lon, az)
            trk.append(
                GX.coord('{0} {1} 0.0'.format(lon, lat))
            )
            trk.append(
                GX.angles(az)
            )
        count += 1

    # Define a view looking at the end point and encompassing the
    # entire time span.
    point = trk.coord[-1].text
    lon, lat, alt = point.split()
    t_start = trk[defns + 'when'][0].text
    t_end = trk[defns + 'when'][-1].text
    view = KML.LookAt(
        GX.TimeSpan(
            KML.begin(t_start)
        ),
        KML.latitude(lat),
        KML.longitude(lon),
        KML.range('1000')
    )
    doc.Document.append(view)
    doc.Document.append(
        KML.Placemark(
            KML.name(title),
            KML.description('''
            Track between {} and {}.
            '''.format(t_start, t_end)),
            KML.styleUrl('#track'),
            trk
        )
    )
    if markers:
        fld = KML.Folder(
            KML.name('Daily Markers'),
            KML.styleUrl('#markers')
        )
        for pm in markers:
            fld.append(pm)
        doc.Document.append(fld)
    return doc


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infile',
                        help='input CSV file')
    parser.add_argument('outfile',
                        help='output KML or KMZ file')
    parser.add_argument('-d', '--decimation',
                        type=int,
                        default=1,
                        help='decimation factor')
    parser.add_argument('-n', '--name',
                        default='GPS Track',
                        help='KML document name')
    parser.add_argument('-t', '--title',
                        default='Latest Position',
                        help='"title" of the latest Placemark in KML file')
    args = parser.parse_args()

    doc = create_document(args.name)
    doc = fill_document(args.infile, doc, inc=args.decimation,
                        title=args.title)
    if args.outfile.endswith('.kmz'):
        with ZipFile(args.outfile, 'w', ZIP_DEFLATED) as kmz:
            open('doc.kml', 'w').write(lxml.etree.tostring(doc,
                                                           pretty_print=True))
            kmz.write('doc.kml')
            os.unlink('doc.kml')
    else:
        open(args.outfile, 'w').write(lxml.etree.tostring(doc,
                                                          pretty_print=True))

if __name__ == '__main__':
    main()
