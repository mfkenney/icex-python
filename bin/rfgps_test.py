#!/usr/bin/env python
#
import cmd
import sys
import re
import os
from xml.etree import ElementTree as ET
from pyicex.rfgps import RfGps, placemark, GpsError
from pyicex.serialcom import Port
from pyicex.geo import Point, great_circle


class TestApp(cmd.Cmd):

    prompt = 'GPS> '
    device = '/dev/ttyUSB0'
    port = None
    gpstable = dict()

    def find_gps(self, address):
        """
        Lookup RfGps instance by unit address.

        :param address: unit address
        :rtype: RfGps
        """
        if self.port is None:
            self.port = Port(self.device, baud=19200)
            self.port.open()
        gps = self.gpstable.setdefault(address, RfGps(self.port, address))
        return gps

    def sample_gps(self, gps):
        try:
            rec = gps.sample(timeout=3)
            if rec and rec.status != 'A':
                self.pfeedback(
                    'WARNING: bad fix from unit {0:d}'.format(rec.address)
                )
        except GpsError as e:
            self.perror(str(e) + '\n')
            rec = None
        return rec

    def poutput(self, msg):
        sys.stdout.write(msg)

    def pfeedback(self, msg):
        sys.stdout.write(msg + '\n')

    def perror(self, msg, statement=None):
        sys.stderr.write(msg)
        if statement:
            sys.stderr.write(' ({0!r})'.format(statement))
        sys.stderr.write('\n')

    def do_sample(self, args):
        """sample N: read sample from unit N"""
        n = int(args.split()[0])
        self.pfeedback('Reading RF-GPS unit {0:d}'.format(n))
        gps = self.find_gps(n)
        drec = self.sample_gps(gps)
        if drec:
            self.poutput('Lat/Lon: {0}/{1}\n'.format(
                drec.lat, drec.lon
            ))
            self.poutput('Vbatt/Ibatt/Temp: {0}v/{1}A/{2} degC\n'.format(
                drec.vbatt, drec.ibatt, drec.temp
            ))
        else:
            self.perror('No response from unit {0:d}'.format(n))

    def do_kml(self, args):
        """kml FILE: write RF-GPS locations to a KML file"""
        if args:
            kml = ET.Element('kml', xmlns='http://www.opengis.net/kml/2.2')
            folder = ET.SubElement(kml, 'Folder')
            ET.SubElement(folder, 'name').text = 'RFGPS'
            ET.SubElement(folder, 'description').text = 'RF-GPS Locations'
            for addr, gps in self.gpstable.items():
                drec = self.sample_gps(gps)
                if drec:
                    folder.append(placemark(drec))
                else:
                    self.perror('No response from unit {0:d}'.format(addr))
            ET.ElementTree(kml).write(args, encoding='utf-8',
                                      xml_declaration=True)

    def default(self, line):
        m = re.match(r'(\d+)->(\d+)', line)
        if m:
            gps0 = self.find_gps(int(m.group(1)))
            gps1 = self.find_gps(int(m.group(2)))
            drec0 = self.sample_gps(gps0)
            drec1 = self.sample_gps(gps1)
            if drec0 and drec1:
                p0 = Point(lat=float(drec0.lat), lon=float(drec0.lon))
                p1 = Point(lat=float(drec1.lat), lon=float(drec1.lon))
                dist, fwd, back = great_circle(p0, p1)
                self.poutput('Range/Bearing: {0:.2f} m / {1:.1f} degT\n'.format(
                    dist, fwd
                ))

    def do_EOF(self, args):
        return True
    do_quit = do_EOF
    do_exit = do_EOF
    do_q = do_EOF

    def emptyline(self):
        pass


if __name__ == '__main__':
    app = TestApp()
    dev = os.environ.get('RFGPS_DEVICE')
    if dev is not None:
        app.device = dev
    app.cmdloop('RF-GPS Test App')
