#!/usr/bin/env python
#
from datetime import datetime
import pytz
import math
import time
import simplejson as json
from decimal import Decimal
from functools import wraps
from xml.etree import ElementTree as ET

__version__ = '2.0'


def coroutine(func):
    @wraps(func)
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.next()
        return cr
    return start


# Unix date/time epoch
epoch = datetime(1970, 1, 1, 0, 0, 0).replace(tzinfo=pytz.utc)


def unix_time(dt):
    """
    Convert a datetime instance to seconds since 1/1/1970 UTC.
    """
    def total_seconds(td):
        return (td.microseconds +
                (td.seconds + td.days * 24 * 3600) * 1e6) / 1e6
    return int(total_seconds(dt - epoch))


def tocsv(record):
    """
    Convert a namedtuple instance to a CSV record.

    :param record: data record object
    :return: CSV formatted string
    """
    cols = []
    for elem in record:
        if isinstance(elem, datetime):
            cols.append(str(unix_time(elem)))
        elif isinstance(elem, str):
            cols.append(repr(elem))
        else:
            cols.append(str(elem))
    return ','.join(cols)


def kml_placemark(drec, name='', style=None):
    """
    Create a KML Placemark element from a GPS or RF-GPS data record. *drec*
    can be any object having *lat*, *lon*, and *timestamp* attributes. The
    *timestamp* attribute can be a :class:`datetime.datetime` object or the
    number of seconds since 1/1/1970 UTC.

    :param drec: data record
    :param name: Placemark name
    :param style: optional style URL
    :return: KML Placemark element
    :rtype: xml.etree.ElementTree.Element
    """
    if isinstance(drec.timestamp, datetime):
        timestamp = drec.timestamp.isoformat()
    else:
        timestamp = time.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00', time.gmtime(drec.timestamp))
    pm = ET.Element('Placemark')
    ET.SubElement(pm, 'name').text = name
    if style:
        ET.SubElement(pm, 'styleUrl').text = style
    ET.SubElement(pm, 'description').text = '{0} location at {1}'.format(
        name, timestamp
    )
    pt = ET.SubElement(pm, 'Point')
    ET.SubElement(pt, 'coordinates').text = '{0},{1},0'.format(
        drec.lon, drec.lat
    )
    ts = ET.SubElement(pm, 'TimeStamp')
    ET.SubElement(ts, 'when').text = timestamp
    return pm


class DataRecordEncoder(json.JSONEncoder):
    """
    Class to encode the GPS and RF-GPS data records
    """
    def default(self, o):
        if isinstance(o, datetime):
            return unix_time(o)
        else:
            return json.JSONEncoder.default(self, o)


class Dmm(object):
    """
    Class to represent angles as degrees and decimal minutes

    >>> Dmm(30.5)
    30-30.0000
    >>> Dmm(-0.5)
    -0-30.0000
    >>> float(Dmm(-0.5))
    -0.5
    >>> abs(Dmm(-0.5))
    0-30.0000
    """
    deg_exp = Decimal('1')
    min_exp = Decimal('0.0001')

    def __init__(self, deg=0):
        self.deg = deg
        if self.deg < 0.:
            self.deg = math.ceil(self.deg)
        elif self.deg > 0.:
            self.deg = math.floor(self.deg)
        self.min = (deg - self.deg) * 60.

    def __str__(self):
        d = Decimal(str(self.deg))
        m = Decimal(str(abs(self.min)))
        padding = (m < 10.) and '0' or ''
        return '%s-%s%s' % (d.quantize(self.deg_exp),
                            padding, m.quantize(self.min_exp))
    __repr__ = __str__

    def __float__(self):
        return self.deg + self.min / 60.

    def is_neg(self):
        return float(self) < 0.

    def __abs__(self):
        d = Dmm()
        d.deg = abs(self.deg)
        d.min = abs(self.min)
        return d


class EMA(object):
    """
    Exponential moving average filter with weighting coefficient *alpha*.
    Filters the input values :math:`Y_i` to produce the output values
    :math:`S_i` such that:

    .. math::

        S_1 = Y_1

        S_i = \\alpha*Y_i+(1-\\alpha)*S_{i-1}

    """
    def __init__(self, alpha):
        self._alpha = alpha
        self._inv = 1. - alpha
        self.s = None

    def __call__(self, y):
        if self.s is None:
            self.s = y
        else:
            self.s = self._alpha * y + self.s * self._inv
        return self.s

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, value):
        self._alpha = value
        self._inv = 1. - value

    def reset(self):
        """
        Reset the filter to its initial state.
        """
        self.s = None
