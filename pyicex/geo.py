#!/usr/bin/env python
#
"""
.. module:: pyicex.geo
     :platform: any
     :synopsis: utilities for geodetic calculations

This module uses the PROJ.4 library for the calculations involved in converting
between Tracking Range grid coordinates and lat-lon::

    >>> p0 = Point(lat=45, lon=-122)
    >>> grid = Grid(p0, 5)
    >>> x = 15000
    >>> y = 15000
    >>> p1 = grid.tolatlon(GridPoint(x=x, y=y))
    >>> r, fwd, back = great_circle(p0, p1)
    >>> round(r, 2)
    21213.18
    >>> round(fwd, 1)
    50.0
    >>> round((x*x + y*y)**0.5, 2)
    21213.2

The calculations above show that the cartesian distance measured on the grid
agrees with the geodetic distance to within 2cm at >20km.
"""
import pyproj
import math
from collections import namedtuple


#: Represents a geodetic position
Point = namedtuple('Point', 'lat lon')
#: Represents a grid position
GridPoint = namedtuple('GridPoint', 'x y')

# GPS projection
wgs84_lla = pyproj.Proj(proj='latlon', datum='WGS84', ellps='WGS84')
# ECEF projection
wgs84_ecef = pyproj.Proj(proj='geocent', datum='WGS84', ellps='WGS84')
# WGS84 ellipsoid
wgs84_geod = pyproj.Geod(ellps='WGS84')


def to_ecef(lat, lon, alt=0.):
    """
    Convert a GPS position to ECEF coordinates.

    :param lat: latitude in degrees
    :param lon: longitude in degrees
    :param alt: altitude in meters
    :return: tuple of X, Y, and Z
    """
    return pyproj.transform(wgs84_lla, wgs84_ecef, lon, lat, alt)


def to_lla(x, y, z):
    """
    Convert an ECEF coordinate to GPS.

    :param x: X coordinate in meters
    :param y: Y coordinate in meters
    :param z: Z coordinate in meters
    :return: tuple of latitude, longitude, and altitude.
    """
    lon, lat, alt = pyproj.transform(wgs84_ecef, wgs84_lla, x, y, z)
    return lat, lon, alt


def great_circle(p0, p1):
    """
    Calculate distance in meters along with the forward and back azimuths for
    the great-circle route from *p0* to *p1*.

    :param p0: starting point
    :type p0: Point
    :param p1: ending point
    :type p1: Point
    :return: tuple of distance (m), forward, and back azimuths (deg)
    """
    fwd, back, dist = wgs84_geod.inv(p0.lon, p0.lat, p1.lon, p1.lat)
    return dist, fwd, back


class Grid(object):
    """
    Class to represent an x-y grid as a Transverse Mercator projection with a
    rotated y-axis.
    """
    def __init__(self, p0, y_azimuth, units='m'):
        """

        :param p0: location of grid origin
        :type p0: Point
        :param y_azimuth: angle of y-axis in degrees True
        :param units: grid coordinate units (*m* or *us-yds*)
        """
        # azimuth is the amount the grid needs to be rotated to align the
        # y-axis with true north.
        self.azimuth = math.radians(360. - float(y_azimuth))
        self._c = math.cos(self.azimuth)
        self._s = math.sin(self.azimuth)
        self.units = units
        self.proj = pyproj.Proj(proj='tmerc', ellps='WGS84',
                                preserve_units=True,
                                units=units,
                                lat_0=float(p0.lat),
                                lon_0=float(p0.lon))

    def toxy(self, p):
        """
        Convert a lat-lon position to grid coordinates.

        :param p: lat-lon position
        :type p: Point
        :return: grid position
        :rtype: GridPoint
        """
        east, north = self.proj(float(p.lon), float(p.lat))
        x = east * self._c + north * self._s
        y = north * self._c - east * self._s
        return GridPoint(x=x, y=y)

    def tolatlon(self, gp):
        """
        Convert a grid coordinate to a lat-lon position.

        :param gp: grid position
        :type gp: GridPoint
        :return: lat-lon position
        :rtype: Point
        """
        x = float(gp.x)
        y = float(gp.y)
        east = x * self._c - y * self._s
        north = y * self._c + x * self._s
        lon, lat = self.proj(east, north, inverse=True)
        return Point(lat=lat, lon=lon)
