#!/usr/bin/env python
#
"""
.. module:: gps
    :platform: any
    :synopsis: interface to an NMEA GPS device
"""
import gevent
from gevent.queue import Queue, Empty
import time
import nmea
import pytz
from array import array
from decimal import Decimal
from collections import namedtuple
from datetime import datetime
from pyicex import coroutine


#: Data record received from the device
GpsData = namedtuple('GpsData',
                     'timestamp lat lon status nsat hdop speed course')
# Used to control the precision of various Decimal values
_exp = [Decimal('1e%d' % i) for i in range(0, -7, -1)]


@coroutine
def read_nmea_sentence(target):
    """
    Coroutine to produce new nmea.Sentence instances from characters
    received from the RF-GPS device.

    :param target: coroutine to consume each nmea.Sentence object
    """
    stx = b'$'
    etx = b'\n'
    while True:
        c = (yield)
        if c != stx:
            continue
        t = time.time()
        contents = array('c')
        contents.append(c)
        while c != etx:
            c = (yield)
            contents.append(c)
        try:
            target.send((t, nmea.Sentence(contents.tostring())))
        except (nmea.BadFormat, nmea.ChecksumError):
            pass


def dm_to_deg(ideg, min):
    """
    Convert degrees and decimal minutes to decimal degrees.

    :param ideg: degrees
    :type ideg: decimal.Decimal
    :param min: minutes
    :type min: decimal.Decimal
    :return: decimal degrees
    :rtype: decimal.Decimal
    """
    return ideg + min / Decimal('60')


def make_data_record(rmc, gga):
    """
    Create a data record from a GPRMC and GPGGA NMEA sentence.

    :param rmc: GPRMC sentence
    :param gga: GPGGA sentence
    :return: new GpsData instance

    >>> import nmea
    >>> rmc = nmea.Sentence('$GPRMC,194253,A,4739.3051,N,12218.9538,W,000.0,000.0,291012,018.2,E,A*02')
    >>> gga = nmea.Sentence(b'$GPGGA,194253,4739.3051,N,12218.9538,W,1,04,5.2,,M,-18.4,M,,*54')
    >>> make_data_record(rmc, gga) # doctest: +NORMALIZE_WHITESPACE
    GpsData(timestamp=datetime.datetime(2012, 10, 29, 19, 42, 53, tzinfo=<UTC>),
    lat=Decimal('47.655085'), lon=Decimal('-122.315897'),
    status='A', nsat=4, hdop=Decimal('5.2'), speed=Decimal('0.0'),
    course=Decimal('0.0'))
    """
    lat = dm_to_deg(Decimal(rmc[2][0:2]), Decimal(rmc[2][2:]))
    if rmc[3] == 'S':
        lat = -lat
    lon = dm_to_deg(Decimal(rmc[4][0:3]), Decimal(rmc[4][3:]))
    if rmc[5] == 'W':
        lon = -lon
    hdop = gga[7] and Decimal(gga[7]) or Decimal('-1.0')
    t = datetime.strptime('{0} {1}'.format(rmc[8], rmc[0]),
                          '%d%m%y %H%M%S').replace(tzinfo=pytz.utc)
    return GpsData(timestamp=t,
                   status=rmc[1],
                   lat=lat.quantize(_exp[6]),
                   lon=lon.quantize(_exp[6]),
                   nsat=int(gga[6]),
                   hdop=hdop.quantize(_exp[1]),
                   speed=Decimal(rmc[6]).quantize(_exp[1]),
                   course=Decimal(rmc[7]).quantize(_exp[1]))


class GarminGps(object):
    """
    Class to represent a Garmin GPS in NMEA mode.
    """
    eol = b'\r\n'
    sentences = ('GPRMC', 'GPGGA')

    def __init__(self, port):
        """

        :param port:
        :type port: serialcom.Port
        """
        assert port.isopen()
        self.port = port
        # Disable all sentences ...
        self.port.write('$PGRMO,,2' + self.eol, timeout=2)
        gevent.sleep(1)
        # ... and enable the ones we need
        for s in self.sentences:
            self.port.write('$PGRMO,{0},1{1}'.format(s, self.eol), timeout=2)
            gevent.sleep(1)
        self.queue = None
        self.reader = None

    @coroutine
    def _store_data(self):
        """
        Coroutine to construct a new GpsData instance.
        """
        rmc = None
        while True:
            t, s = (yield)
            if s.id == 'GPRMC':
                rmc = s
            elif s.id == 'GPGGA':
                if rmc and rmc[0] == s[0]:
                    try:
                        rec = make_data_record(rmc, s)
                    except (ValueError, TypeError):
                        pass
                    else:
                        if self.queue.full():
                            self.queue.get()
                        self.queue.put((t, rec))
                    rmc = None

    def _reader(self):
        scanner = read_nmea_sentence(self._store_data())
        while True:
            try:
                buf = self.port.read(80)
            except IOError:
                break
            for c in buf:
                scanner.send(c)

    def read(self, timeout=None):
        """
        Read the next data record from the device.

        :param timeout: maximum number of seconds to wait
        :raises: IOError on timeout
        """
        assert self.reader is not None
        try:
            _, data = self.queue.get(timeout=timeout)
        except Empty:
            raise IOError((-1, 'No data from GPS'))
        return data

    def start(self):
        """
        Start the reader thread
        """
        self.queue = Queue(maxsize=1)
        self.reader = gevent.spawn(self._reader)

    def stop(self):
        """
        Stop the reader thread
        """
        if self.reader:
            self.reader.kill(timeout=3)
            self.reader = None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

if __name__ == '__main__':
    from pyicex.serialcom import Port
    from sys import argv
    p = Port(argv[1], baud=4800)
    p.open()
    with GarminGps(p) as g:
        for i in range(10):
            print g.read()
    p.close()
