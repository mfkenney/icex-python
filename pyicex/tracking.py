#!/usr/bin/env python
#
"""
.. module:: tracking
    :platform: any
    :synopsis: module for working with ICEX tracking data
"""
from datetime import datetime
from decimal import Decimal
from collections import namedtuple
import pytz


# Data records
TxyData = namedtuple('TxyData', 'timestamp x y tt')
TsData = namedtuple('TsData', 'timestamp freq quad tt')


# Used to control the precision of various Decimal values
_exp = [Decimal('1e%d' % i) for i in range(0, -7, -1)]

# Date/time format
TIMEFMT = '%d/%m/%Y %H:%M:%S'


def parse_txy(line):
    """
    Generator to extract the :class:`TxyData` data records from a
    single line of a TXY file.
    """
    f = line.strip().split()
    t = datetime.strptime('{0} {1}'.format(f[0], f[1]),
                          TIMEFMT).replace(tzinfo=pytz.utc)
    if f[4] != '999':
        x = Decimal(f[2])
        y = Decimal(f[3])
        tt = [Decimal('-1')] * 3
        for idx, val in zip([int(i) - 1 for i in f[4]], f[5:8]):
            tt[idx] = Decimal(val)
        yield TxyData(timestamp=t, x=x, y=y, tt=tt)
    if f[10] != '999':
        x = Decimal(f[8])
        y = Decimal(f[9])
        tt = [Decimal('-1')] * 3
        for idx, val in zip([int(i) - 1 for i in f[10]], f[11:14]):
            tt[idx] = Decimal(val)
        yield TxyData(timestamp=t, x=x, y=y, tt=tt)


def parse_ts(line, wantall=True):
    """
    Generator to extract the :class:`TsData` data records from a single
    line of a time-stamp log file.

    :param line: single line from the time-stamp log file
    :param wantall: if false, only return records with returns from
                    at least three hydrophones.
    """
    f = line.strip().split()
    if len(f) != 18:
        return
    t = datetime.strptime('{0} {1}'.format(f[0], f[1]),
                          TIMEFMT).replace(tzinfo=pytz.utc)
    idx = 2
    for freq, quad in [(1, 1), (1, 2), (2, 1), (2, 2)]:
        tt = [Decimal(x) for x in f[idx:idx+4]]
        valid = 0
        for i in range(4):
            if tt[i] > 0:
                tt[i] = tt[i] * _exp[5]
                valid += 1
        if wantall or valid > 2:
            yield TsData(timestamp=t, freq=freq, quad=quad, tt=tt)
        idx += 4


def read_survey(infile):
    """
    Read the contents of a survey reference file (a '*.xyz file). The
    result is returned as a 8x2 matrix where each row contains the
    X-Y position of a hydrophone.

    :param infile: input file object.
    """
    xy = []
    for line in infile:
        f = line.strip().split(', ')
        xy.append([float(f[2]), float(f[3])])
    return xy
