#!/usr/bin/env python
#
"""
.. module:: met
    :platform: Any
    :synopsis: interface to Weatherpack WP-2000 met station

This module contains functions and classes for interfacing with a Weatherpack
WP-2000 Met Station. It expects the device to be configured to output a CSV
data record with the following variables:

.. _datarec:

* unit serial number
* sample date
* sample time
* average wind speed (m/s)
* average wind direction (degrees)
* wind gust (m/s)
* wind direction standard deviation (degrees)
* air temperature (deg C)
* barometric pressure (mb)
* relative humidity (%)
* battery voltage (V)

"""
import gevent
import time
import pytz
from gevent.queue import Queue, Empty, Full
from collections import namedtuple
from decimal import Decimal
from string import whitespace
from datetime import datetime
from pyicex import coroutine

#: Data record received from the device.
MetData = namedtuple('MetData',
                     'timestamp awspd awdr gust sd temp bp rh vbatt')
# Used to control the precision of various Decimal values
_exp = [Decimal('1e{:d}'.format(i)) for i in range(0, -7, -1)]


@coroutine
def delimiter_scanner(target, delim):
    """
    Coroutine to scan data records ended by a delimiter

    :param target: coroutine to accept data records
    :param delim: delimiter string
    """
    n = len(delim)
    t = None
    while True:
        contents = bytearray()
        c = (yield)
        if t is None:
            t = time.time()
        contents.append(c)
        while contents[-n:] != delim:
            c = (yield)
            contents.append(c)
        target.send((t, str(contents[:-n])))
        t = None


def make_data_record(t, L):
    """
    Convert a timestamp and a list of strings into a
    :ref:`data record <datarec>`. The timestamp is from the
    computer clock, the date/time returned from the device
    is ignored.

    :param t: data timestamp in seconds since the epoch
    :param L: data record fields as a list of strings
    :rtype: MetData

    >>> line = '2601,11/03/10,16:40:15,6.3,130,6.7,6.0,-24.5,1020,77,12.6'
    >>> make_data_record(0, line.split(',')) # doctest: +NORMALIZE_WHITESPACE
    MetData(timestamp=datetime.datetime(1970, 1, 1, 0, 0, tzinfo=<UTC>),
    awspd=Decimal('6.3'), awdr=Decimal('130'), gust=Decimal('6.7'),
    sd=Decimal('6.0'), temp=Decimal('-24.5'), bp=Decimal('1020'),
    rh=Decimal('77'), vbatt=Decimal('12.6'))
    """
    dt = datetime.utcfromtimestamp(t).replace(tzinfo=pytz.utc)
    return MetData(timestamp=dt,
                   awspd=Decimal(L[3]).quantize(_exp[1]),
                   awdr=Decimal(L[4]).quantize(_exp[0]),
                   gust=Decimal(L[5]).quantize(_exp[1]),
                   sd=Decimal(L[6]).quantize(_exp[1]),
                   temp=Decimal(L[7]).quantize(_exp[1]),
                   bp=Decimal(L[8]).quantize(_exp[0]),
                   rh=Decimal(L[9]).quantize(_exp[0]),
                   vbatt=Decimal(L[10]).quantize(_exp[1]))


class Wp2000(object):
    """
    Class to represent the Wp2000 met station.

    This class does not handle the device configuration, that needs to
    be done manually with a terminal emulator. The device should be
    configured to output :ref:`data records <datarec>` on a periodic
    basis.

    A light-weight thread (Greenlet) is used to read the device output
    and add records to a queue. The user can obtain the data records by
    calling the :meth:`read` method::

        met = Wp2000(port)
        # Start the reader thread
        met.start()
        while some_condition:
            data = met.read()
            process_data(data)
        met.stop()

    This class also supports the *context management protocol*::

        with Wp2000(port) as met:
            while some_condition:
                process_data(met.read())

    """
    eol = '\n'
    strip_chars = whitespace + ','

    def __init__(self, port, sn='2601'):
        """

        :param port:
        :type port: serialcom.Port
        """
        assert port.isopen()
        self.port = port
        # This device requires that the DTR be asserted before it will
        # transmit on the serial line (old-school)
        self.port.serial.setDTR(True)
        self.queue = None
        self.reader = None
        self.timeout = None
        self.sn = sn

    @coroutine
    def _enqueue(self):
        """
        Accept lines of input from the scanner coroutine. If the line
        represents a data record, parse it to a MetData instance and
        append to the input queue.
        """
        while True:
            t, line = (yield)
            cols = line.strip(self.strip_chars).split(',')
            if len(cols) > 1 and cols[0] == self.sn:
                try:
                    self.queue.put(make_data_record(t, cols))
                except (Full, ValueError):
                    pass

    def _reader(self):
        """
        Task to read characters from the serial port and pass them
        to the scanner coroutine.
        """
        scanner = delimiter_scanner(self._enqueue(), self.eol)
        while True:
            try:
                buf = self.port.read(80)
            except IOError:
                break
            for c in buf:
                scanner.send(c)

    def start(self, timeout=None):
        """
        Start the reader thread
        """
        self.timeout = timeout
        self.queue = Queue()
        self.reader = gevent.spawn(self._reader)

    def stop(self):
        """
        Stop the reader thread
        """
        if self.reader:
            self.reader.kill(timeout=3)
            self.reader = None

    def read(self, timeout=None):
        """
        Read the next data record from the input queue.

        :param timeout: maximum number of seconds to wait,
                        ``None`` waits forever.
        :rtype: MetData
        """
        assert self.reader is not None
        try:
            data = self.queue.get(timeout=timeout)
        except Empty:
            raise IOError((-1, 'Timeout reading data record'))
        else:
            return data

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()
