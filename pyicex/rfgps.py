#!/usr/bin/env python
#
"""
.. module:: rfgps
    :platform: Any
    :synopsis: interface to the ICEX RF-GPS devices
"""
from array import array
import gevent
from gevent.queue import Queue, Empty
from collections import namedtuple
from decimal import Decimal
from datetime import datetime
import pytz
from pyicex import coroutine, kml_placemark


class GpsError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return 'GpsError(%r)' % self.msg


#: Data record received from the device
RfgpsData = namedtuple('RfgpsData',
                       'address timestamp lat lon status vbatt ibatt temp')
# Used to control the precision of various Decimal values
_exp = [Decimal('1e%d' % i) for i in range(0, -7, -1)]


def list_to_datarecord(L):
    """
    Convert a raw sample from the RF-GPS, stored as a list of strings, to a
    RfgpsData instance.

    >>> s = '001,091012,151400,47.123456,-122.567890,A,12.34,0.211,17.2'
    >>> list_to_datarecord(s.split(',')) # doctest: +NORMALIZE_WHITESPACE
    RfgpsData(address=1, timestamp=datetime.datetime(2012, 10, 9, 15, 14,
    tzinfo=<UTC>), lat=Decimal('47.123456'), lon=Decimal('-122.567890'),
    status='A', vbatt=Decimal('12.34'), ibatt=Decimal('0.211'),
    temp=Decimal('17.2'))
    """
    return RfgpsData(address=int(L[0], 2),
                     timestamp=datetime.strptime('{0} {1}'.format(L[1], L[2]),
                                                 '%d%m%y %H%M%S'
                     ).replace(tzinfo=pytz.utc),
                     lat=Decimal(L[3]).quantize(_exp[6]),
                     lon=Decimal(L[4]).quantize(_exp[6]),
                     status=L[5],
                     vbatt=Decimal(L[6]).quantize(_exp[2]),
                     ibatt=Decimal(L[7]).quantize(_exp[3]),
                     temp=Decimal(L[8]).quantize(_exp[1]))


def datarecord_to_list(d):
    """
    Convert a RfgpsData instance to a list of strings.

    :param d: data record
    :type d: RfgpsData
    """
    date, time = d.timestamp.strftime('%d%m%y %H%M%S').split()
    L = list()
    L.append('{0:03b}'.format(d.address))
    L.append(date)
    L.append(time)
    for e in d[2:]:
        L.append(str(e))
    return L


def placemark(drec, style=None):
    """
    Use RfgpsData contents to generate a KML Placemark element.

    :param drec: RF-GPS data record
    :type drec: RfgpsData
    :param style: optional style URL
    :return: KML Placemark element
    :rtype: xml.etree.ElementTree.Element
    """
    return kml_placemark(
        drec, name='RF-GPS-{0:03d}'.format(drec.address), style=style)


@coroutine
def read_record(target, stx='$', etx='\n'):
    """
    Coroutine to produce new data records from characters received
    from the RF-GPS device.

    :param target: coroutine to consume each data record
    :param stx: record start character
    :param etx: record end character
    """
    while True:
        c = (yield)
        if c != stx:
            continue
        contents = array('c')
        while c != etx:
            c = (yield)
            contents.append(c)
        target.send(contents.tostring())


data_queues = {}
def reader(port, stx='$', etx='\n'):
    """
    Greenlet task to read and parse data records from the RF-GPS. Bytes are
    read from a serial port and data records are sent to the proper queue (
    one queue for each device address).
    """
    @coroutine
    def get_frame():
        while True:
            frame = (yield)
            fields = frame.strip().split(',')
            queue = data_queues.get(int(fields[0], 2))
            if queue:
                if fields[1] == '-1':
                    queue.put(GpsError(fields[2]))
                else:
                    try:
                        queue.put(list_to_datarecord(fields))
                    except Exception:
                        pass

    scanner = read_record(get_frame(), stx=stx, etx=etx)
    while True:
        try:
            buf = port.read(64)
        except IOError:
            break
        for c in buf:
            scanner.send(c)


class RfGps(object):
    """
    Class to represent a single RF-GPS device.

    These devices are on a shared serial RF network and have a unique 3-digit
    address. The following commands are available::

        $Rnnn -- put unit nnn in auto 1Hz output mode
        $Pnnn -- put unit nnn in polled mode
        $Snnn -- request a new sample from unit nnn when in polled mode

    Each sample is formatted as a single CSV record starting with a ``$``,
    ending with a ``CR+LF`` and containing the following fields:

      address
        device address
      date
        fix date as ddmmyy
      time
        fix time as hhmmss
      latitude
        GPS latitude as [-]dd.dddddd
      longitude
        GPS longitude as [-]ddd.dddddd
      fix status
        'A' for valid fix, 'V' for invalid
      vbatt
        battery voltage in volts as vv.vv
      ibatt
        battery current in amps as i.ii
      temperature
        temperature in degrees C as tt.t

    If the controller detects an error, the first two fields are as above
    and the third field contains a text error message describing the problem.
    """
    _readers = {}
    _queues = {}
    stx = b'$'
    etx = b'\n'
    eol = b'\r\n'

    def __new__(cls, port, address):
        assert port.isopen()
        obj = object.__new__(cls)
        # Create one Queue for each address
        if address not in data_queues:
            data_queues[address] = Queue()
        obj.queue = data_queues[address]
        # Spawn a reader for each unique serial port
        if port not in cls._readers:
            cls._readers[port] = gevent.spawn(reader, port,
                                              stx=cls.stx, etx=cls.etx)
        obj.reader = cls._readers[port]
        return obj

    def __init__(self, port, address):
        """
        .. note::

           port must be open.

        :param port: serial port interface
        :type port: serialcom.Port
        :param address: device address
        """
        self.port = port
        self.address = address
        # Make sure the unit is in polled-mode
        self.port.write('$P{0:03d}{1}'.format(self.address, self.eol),
                        timeout=1)

    def flush_input(self):
        """
        Flush all data from the input queue.
        """
        try:
            while True:
                self.queue.get_nowait()
        except Empty:
            pass

    def sample(self, timeout=None):
        """
        Send an ``Snnn`` command and read the reply.

        :param timeout: maximum time to wait in seconds
                        (if ``None`` wait forever)
        :return: data record or ``None`` on timeout
        :rtype: RfgpsData
        :raises: GpsError
        """
        self.flush_input()
        self.port.write('$S{0:03b}{1}'.format(self.address, self.eol),
                        timeout=timeout)
        try:
            rec = self.queue.get(timeout=timeout)
            if isinstance(rec, GpsError):
                raise rec
            else:
                return rec
        except Empty:
            return None
