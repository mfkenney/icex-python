#!/usr/bin/env python
#
"""
.. module:: survey
   :platform: any
   :synopsis: survey calculations for ICEX tracking range
"""
import numpy as np
import math
from geo import great_circle


def ranges(points):
    r"""
    Create a matrix of ranges and a matrix of azimuths between GPS positions

    .. math::

        R_{ij} = \text{ range in meters between positions i and j}

        az_{ij} = \text{ true azimuth from position i to j}

    This data is used to find the best-fit X-Y coordinates for each position.
    While we could calculate X and Y directly (using the bearing) we cannot
    average the values because the floe is moving. In contrast, the range
    values should remain constant in time and thus can be averaged to reduce
    the noise in the GPS readings.

    :param points: list of :class:`geo.Point` objects
    :return: tuple of ``R`` and ``az``
    """
    n = len(points)
    R = np.zeros((n, n))
    az = np.zeros((n, n))
    for i in range(n):
        for j in range(i + 1, n):
            dist, fwd, back = great_circle(points[i], points[j])
            R[i, j] = dist
            R[j, i] = dist
            az[i, j] = fwd
            az[j, i] = back
    return R, az


def xyfit(R):
    """
    Given a matrix of ranges (created by :func:`ranges`) between 4 RF-GPS
    units placed at the ICEX hydrophone locations, calculate the X,Y
    coordinates for each unit. The origin is placed at hydrophone-4 and the
    Y-axis is the line between hydrophones 1 and 2.

    :param R: range matrix
    :type R: numpy.array
    :return: numpy array of X,Y coordinates
    :rtype: numpy.array
    """
    assert R.shape == (4, 4)
    d12 = R[0, 1]
    d23 = R[1, 2]
    d31 = R[2, 0]
    d14 = R[0, 3]
    d24 = R[1, 3]
    d34 = R[2, 3]

    d = (d23**2 - d31**2 + d12**2) / 2. / d12
    e = d12 - d
    a = math.sqrt(d31**2 - e**2)
    d9 = (d24**2 - d14**2 + d12**2) / 2. /d12
    e9 = d12 - d9
    a9 = math.sqrt(d14**2 - e9**2)
    e8 = e - e9

    coords = np.zeros((4, 2), np.float)
    # Unit 1
    coords[0, 0] = -a9
    coords[0, 1] = e - e8

    # Unit 2
    coords[1, 0] = -a9
    coords[1, 1] = -d - e8

    # Unit 3
    coords[2, 0] = a - a9
    coords[2, 1] = -e8
    return coords


def range2xyz(refs, r):
    """
    Given a set of 3 reference X,Y locations and a set of ranges to a target,
    calculate the X,Y,Z location of the target along with the bearing from
    the +Y axis. The reference points are assumed to have the same Z coordinate;
    the Z coordinate of the target will be relative to this.

    We are solving the following set of simultaneous equations for X, Y, and Z.

    .. math::

        {r_i}^2 = {(x - refs_{i0})}^2 + {(y - refs_{i1})}^2 + z^2

    :param refs: a (3, 2) array of reference point (hydrophone) X,Y
                 locations.
    :param r: a 3-element array of ranges to a target
    :return: a tuple containing X, Y, Z, bearing of the target
    """
    assert refs.shape == (3, 2)
    assert len(r) == 3
    # This calculation blows up if the first two reference points
    # have the same Y coordinate
    if refs[0, 1] == refs[1, 1]:
        raise ValueError

    refs_sq = refs * refs
    r_sq = r * r
    a = (r_sq[0] - r_sq[1] + refs_sq[1, 0] - refs[0, 0] + refs_sq[1, 1] - refs_sq[0, 1]) / 2.
    b = (r_sq[1] - r_sq[2] + refs_sq[2, 0] - refs[1, 0] + refs_sq[2, 1] - refs_sq[1, 1]) / 2.
    xc = (a * (refs[2, 1] - refs[1, 1]) - b * (refs[1, 1] - refs[0, 1])) \
        / ((refs[1, 0] - refs[0, 0]) * (refs[2, 1] - refs[1, 1])
            - (refs[2, 0] - refs[1, 0]) * (refs[1, 1] - refs[0, 1]))
    yc = (a - xc * (refs[1, 0] - refs[0, 0])) / (refs[1, 1] - refs[0, 1])
    z_sq = r_sq[0] - (xc - refs[0, 0])**2 - (yc - refs[0, 1])**2
    zc = math.copysign(abs(z_sq)**0.5, z_sq)
    # Bearing is measure from the +Y axis
    bearing = math.atan2(xc, yc)
    return xc, yc, zc, math.degrees(bearing)


def trilateration(refs, r):
    """
    Alternative method for range2xyz as defined on the Wikipedia page
    for Trilateration_.

    .. _Trilateration: http://en.wikipedia.org/wiki/Trilateration

    We are solving the following set of simultaneous equations for X, Y, and Z.

    .. math::

        {r_i}^2 = {(x - refs_{i0})}^2 + {(y - refs_{i1})}^2 + z^2

    :param refs: a (3, 2) array of reference point (hydrophone) X,Y
                 locations.
    :param r: a 3-element array of ranges to a target
    :return: a tuple containing X, Y, Z, bearing of the target
    """
    # Transform the coordinate system so the positive X-axis runs
    # from refs[0] to refs[1] with the origin at refs[0].
    #
    # Calculate the X unit vector
    e_hat_x = (refs[1] - refs[0])
    d = np.dot(e_hat_x, e_hat_x)**0.5
    e_hat_x = e_hat_x / d
    # X-coordinate of refs[2]
    i = np.dot(e_hat_x, refs[2] - refs[0])
    # Y unit vector
    e_hat_y = refs[2] - refs[0] - i*e_hat_x
    e_hat_y = e_hat_y / (np.dot(e_hat_y, e_hat_y)**0.5)
    # Y-coordinate of refs[2]
    j = np.dot(e_hat_y, refs[2] - refs[0])
    # Z unit vector
    e_hat_z = np.cross(e_hat_x, e_hat_y)
    # Calculate the target coordinates
    r_sq = r * r
    xc = (r_sq[0] - r_sq[1] + d**2) / (2*d)
    yc = (r_sq[0] - r_sq[2] + i**2 + j**2) / (2*j) - (i * xc) / j
    z_sq = r_sq[0] - xc**2 - yc**2
    zc = math.copysign(abs(z_sq)**0.5, z_sq)
    # Transform back to original coordinate system
    p = refs[0] + xc*e_hat_x + yc*e_hat_y
    x = p[0]
    y = p[1]
    z = zc*e_hat_z
    bearing = math.atan2(x, y)
    return x, y, z, math.degrees(bearing)
