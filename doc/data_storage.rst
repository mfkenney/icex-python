ICEX Data Storage
=================

Data Record Formats
-------------------

Listed below are the definitions of the main data records used by the ICEX
programs. JSON_ is used to serialize all data records.

.. _JSON: http://json.org

GPS
  Origin GPS fix::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      lat: <latitude in decimal degrees>,
      lon: <longitude in decimal degrees>,
      status: <fix status, 'A' or 'V'>,
      nsat: <number of satellites in view>,
      hdop: <horizontal dilution of precision>,
      course: <course in degrees True>,
      speed: <speed in knots>
    }

RF-GPS
  Fix information from an RF-GPS unit::

    {
      address: <unit address>,
      timestamp: <seconds since 1/1/1970 UTC>,
      lat: <latitude in decimal degrees>,
      lon: <longitude in decimal degrees>,
      status: <fix status, 'A' or 'V'>,
      vbatt: <battery voltage in volts>,
      ibatt: <battery current in amps>,
      temp: <internal temperature in degrees C>
    }

Grid
  Grid orientation record. The reference point names have the form
  *RF-GPS-N*, where *N* is the device address::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      lat: <origin latitude in decimal degrees>,
      lon: <origin longitude in decimal degrees>,
      azimuth: <y-axis azimuth in degrees True>,
      refs: [
              {
               name: <ref-point name>,
               range: <range change in us-yds>,
               bearing: <bearing change in degrees>
              },
              .
              .
              .
            ]
    }

Survey Points
  Range survey reference points::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      points: [
                 {
                    address: <RF-GPS address>,
                    name: <hydrophone name>,
                    x: <x-coordinate in yards>,
                    y: <y-coordinate in yards>
                 },
                 .
                 .
                 .
              ]
    }

Met Station
  Data from the weather station::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      awspd: <average wind speed in m/s>,
      awdr: <average wind direction in degrees>,
      gust: <wind gust in m/s>,
      sd: <standard deviation of wind direction in degrees>,
      temp: <air temperature in degrees C>,
      bp: <barometric pressure in millibars>,
      rh: <relative humidity in percent>,
      vbatt: <battery voltage in volts>
    }

Point of Interest
  This data record represents an "annotated" grid point::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      x: <x-coordinate in yards>,
      y: <y-coordinate in yards>,
      name: <short name for POI (no spaces)>,
      desc: <optional text description>
    }


Ranges
  This data record contains the range and azimuth matrices required for
  the range survey::

    {
      timestamp: <seconds since 1/1/1970 UTC>,
      R: <5x5 matrix, range between points i and j>,
      az: <5x5 matrix, azimuth from point i to j>,
      addrs: [<list of RF-GPS addresses>]
    }


Redis Contents
--------------

Database Entries
~~~~~~~~~~~~~~~~

gps:current
  The most recent Origin GPS fix.

grid:current
  The most recent grid orientation record.

survey
  The list of range survey reference points.

weather.history
  A list of the most recent Met Station samples.

rfgps:map
  A has which maps RF-GPS addresses to hydrophone names (e.g. H1, H2, etc)

Channels
~~~~~~~~

These pubsub channels allow multiple processes to access the real-time data.

data.gps
  Publishes GPS data records

data.rfgps
  Publishes RF-GPS data records

data.grid
  Publishes a combined set of GPS and RF-GPS data needed for calculating
  the grid orientation.

data.weather
  Publishes Met Station data records

grid.update
  Publishes grid-orientation data records

grid.alert
  Publishes alert messages (e.g. if RF-GPS range changes significantly)

data.ranges
  Publishes the ranges and true azimuths between the Origin GPS and all of the
  RF-GPS units during survey mode.

ranges.filtered
  This channel publishes a filtered version of *data.ranges*.

poi
  Publishes Point of Interest data records.

CSV File Formats
----------------

GPS Columns
~~~~~~~~~~~

* timestamp (seconds since 1/1/1970 UTC)
* latitude (decimal degrees)
* longitude (decimal degrees)
* fix status ("A" for valid, "V" for NAV receiver warning)
* satellite count
* horizontal dilution of precision
* speed estimate (knots)
* course (degrees true)

RF-GPS Columns
~~~~~~~~~~~~~~

* unit address
* timestamp (seconds since 1/1/1970 UTC)
* latitude (decimal degrees)
* longitude (decimal degrees)
* fix status ("A" for valid, "V" for NAV receiver warning)
* battery voltage (volts)
* current (amps)
* temperature (degrees C)

Grid Columns
~~~~~~~~~~~~

* timestamp (seconds since 1/1/1970 UTC)
* latitude (decimal degrees)
* longitude (decimal degrees)
* azimuth (degrees true)

Met Data Columns
~~~~~~~~~~~~~~~~

* timestamp (seconds since 1/1/1970 UTC)
* average wind speed (m/s)
* average wind direction (degrees)
* wind gust (m/s)
* wind direction standard deviation (degrees)
* air temperature (deg C)
* barometric pressure (mb)
* relative humidity (%)
