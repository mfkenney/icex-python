ICEX Data Collection
====================

This document describes the software used to collect and archive the auxiliary
data (GPS and Met Station) for the ICEX Tracking Range. The new software
separates the data collection from the data display.

Currently, this software will only run under Linux or Unix (e.g. OSX). A
separate User's Manual describes the installation process and the user
interface.

Data Flow
---------

The data-collection system is composed of multiple processes communicating
through a Redis_ key-value store. Redis provides a simple database, message
queues, and pubsub_ channels. The block diagram below shows how the collected
data flows through the system.

.. _Redis: http://redis.io/
.. _pubsub: http://en.wikipedia.org/wiki/PubSub

.. blockdiag::

  blockdiag {
    RFGPS [shape = "roundedbox", stacked];
    GPS [shape = "roundedbox"];
    METST [shape = "roundedbox", label = "MET STATION"];
    CHK [label = "GRID CHECK"];
    FILT [label = "RANGE FILTER"];
    READER [label = "GPS READER"];
    METRDR [label = "MET READER"];
    ARCHIVE [shape = "flowchart.database", stacked];
    RFGPS,GPS -> READER;
    METST -> METRDR;
    READER -> ARCHIVER,CHK,FILT;
    CHK -> ARCHIVER;
    METRDR -> ARCHIVER;
    ARCHIVER -> ARCHIVE;
  }

Note that all of the data flows via pubsub channels and thus, other
processes can access the data in real-time (e.g. for user-interface
display).

Processes
---------

GPS Reader
~~~~~~~~~~

The GPS Reader process reads the Origin GPS and all of the RF-GPS
units. It publishes messages on the following channels.

+-------------+-------------------------------------------------------+
| **Channel** | **Description**                                       |
+-------------+-------------------------------------------------------+
| data.gps    | Origin GPS data records                               |
+-------------+-------------------------------------------------------+
| data.rfgps  | RF-GPS data records (separate message from each unit) |
+-------------+-------------------------------------------------------+
| data.grid   | Combined GPS/RF-GPS for grid calculations             |
+-------------+-------------------------------------------------------+
| data.ranges | Ranges and azimuths between all GPS's (for survey)    |
+-------------+-------------------------------------------------------+


Grid Check
~~~~~~~~~~

The Grid Check subscribes to the *data.grid* channel and uses the
information to perform the Range Check Procedure. The output of this is
the new grid origin and azimuth which are published to the *grid.update*
channel.

This task also monitors the range from the Origin to each RF-GPS unit. If
this range exceeds a configured threshold, a warning message is published
to the *grid.alert* channel.

Range Filter
~~~~~~~~~~~~

This process subscribes to the *data.ranges* channel and applies a
moving-average filter to the data. The filtered range data is published to
the *ranges.filtered* channel.

Met Reader
~~~~~~~~~~

This process reads the data records from the Met Station and publishes
them to the *data.weather* channel. The most recent *N* records can also
be found in the *weather.history* message queue.

Archiver
~~~~~~~~

The Archiver subscribes to the *data.gps*, *data.rfgps*, *grid.check*, and
*data.weather* channels and stores the message contents under an archive
directory.

Data Archive
------------

Each data type is archived in a series of CSV files stored under a unique
directory. A new data file will be created on a schedule defined by a
configuration file. In the diagram below, a new file is created each day::

  $DATAROOT/
       |
       +- MET/
       |   |
       |   + met_YYYYMMDD.csv
       |   |
       |   + ...
       |
       +- GPS/
       |   |
       |   + gps_YYYYMMDD.csv
       |   |
       |   + ...
       |
       +- RF-GPS/
       |   |
       |   + rfgps_YYYYMMDD.csv
       |   |
       |   + ...
       |
       +- GRID/
           |
           + grid_YYYYMMDD.csv
           |
           + ...
