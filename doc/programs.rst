Data Collection Software
========================


archiver.py
-----------

This program manages the ICEX data archive. It monitors a series of Redis_
pub-sub channels and writes the message contents to corresponding CSV
files. New archive files are created on a schedule which is determined by
the configuration file.

.. _Redis: http://redis.io/

  archiver.py CFGFILE

.. program:: archiver.py

.. describe:: CFGFILE

   A YAML format configuration file which specifies the message channel,
   archive file name templates, and the file creation schedule.

Configuration File
~~~~~~~~~~~~~~~~~~

The default configuration file is shown below. A new archive files is
created each day (86400 seconds) on the day boundary (i.e. at midnight).

.. code-block:: yaml

  ---
  # Describe the Redis server hosting the message queue
  mq:
    host: localhost
    port: 6379
    db: 0
  # The following list has a entry for each archive
  archives:
    - channel: data.gps
      dir: ~/data/ICEX/GPS
      template: gps_%Y%m%d.csv
      rollover: 86400
      header: [time, lat, lon, status, nsat, hdop, speed, course]
    - channel: data.rfgps
      dir: ~/data/ICEX/RF-GPS
      template: rfgps_%Y%m%d.csv
      rollover: 86400
      header: [address, time, lat, lon, status, vbatt, current, temp]
    - channel: grid.update
      dir: ~/data/ICEX/GRID
      template: grid_%Y%m%d.csv
      rollover: 86400
      header: [time, lat, lon, azimuth]
    - channel: data.weather
      dir: ~/data/ICEX/MET
      template: met_%Y%m%d.csv
      rollover: 86400
      header: [time, awspd, awdr, gust, sd, temp, bp, rh, vbatt]

gps_reader.py
-------------

This program reads the Origin GPS and the RF-GPS units on a periodic
basis. It generates grid-check data records (read by ``gridcheck.py``, see
below) and range data records (used for the range survey).

  gps_reader.py [-o KEY=VALUE] [CFGFILE]

.. program:: gps_reader.py

.. cmdoption:: -o <KEY=VALUE>

   Override configuration file settings, (e.g. ``-o params.rfgps_check=30``).

.. describe:: CFGFILE

   A YAML format configuration file (see below).

Configuration File
~~~~~~~~~~~~~~~~~~

The default configuration file is shown below.

.. code-block:: yaml

  ---
  mq:
  host: localhost
  port: 6379
  db: 0
  gps:
    device: /dev/ttyS1
    baud: 4800
  rfgps:
    device: /dev/ttyS2
    baud: 19200
    addresses: [0, 1, 2, 3]
  params:
    # Time in seconds between RF-GPS samples. Set to 0 to
    # disable sampling.
    rfgps_check: 10


gridcheck.py
------------

This program reads the list of survey reference points at startup. It then
monitors the *data.grid* for messages from the GPS Reader and uses this
information to calculate a new grid azimuth and check for range deviations
in the survey reference points.

It publishes the new grid information to the *grid.update* channel and
publishes alerts to *grid.alerts* if any reference point moves outside of
a defined watch circle.

Note that this program must be restarted after a new grid survey.

  gridcheck.py [-o KEY=VALUE] [CFGFILE]

.. program:: gridcheck.py

.. cmdoption:: -o <KEY=VALUE>

   Override configuration file settings, (e.g. ``-o watch=10``).

.. describe:: CFGFILE

   A YAML format configuration file (see below).

Configuration File
~~~~~~~~~~~~~~~~~~

The default configuration file is shown below.

.. code-block:: yaml

  --
  mq:
    host: localhost
    port: 6379
    db: 0
  # Database key containing the survey reference points.
  dbkey: survey
  # Distance units
  units: us-yd
  # Watch circle radius in "units"
  watch: 2
  # Parameter for the exponential moving average filter that
  # is applied to the measured ranges before the watch-circle
  # check. The default value corresponds to an averaging
  # interval of approximately 10 samples.
  alpha: 0.1


met_reader.py
-------------

Samples the Met Station.

  met_reader.py [-o KEY=VALUE] [CFGFILE]

.. program:: met_reader.py

.. cmdoption:: -o <KEY=VALUE>

   Override configuration file settings, (e.g. ``-o params.history=240``).

.. describe:: CFGFILE

   A YAML format configuration file (see below).

Configuration File
~~~~~~~~~~~~~~~~~~

The default configuration file is shown below.

.. code-block:: yaml

  --
  mq:
    host: localhost
    port: 6379
    db: 0
  met:
    device: /dev/ttyUSB0
    baud: 9600
  params:
    history: 1440

range_filter.py
---------------

Applies an `exponential moving average filter`_ to the range data records
generated by the GPS Reader.

.. _`exponential moving average filter`: https://en.wikipedia.org/wiki/Exponential_smoothing

  range_filter.py [--alpha COEFF]

.. program:: range_filter.py

.. cmdoption:: --alpha COEFF

   Specify the filter :math:`\alpha` coefficient, the filter time constant
   (in samples) is approximately equal to :math:`\frac{1}{\alpha}`. The
   default coefficient value is 0.125.
