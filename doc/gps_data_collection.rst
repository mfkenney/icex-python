ICEX GPS Data Collection
=========================

The ICEX tracking range X-Y coordinate system is a rotated Transverse Mercator projection
defined by an origin latitude/longitude (:math:`\phi_0, \lambda_0`) and the azimuth
(:math:`\alpha`) of the Y axis measured clockwise from true north.

Conversions
-----------

The PROJ.4_ Cartographic Projections Library is used to convert between latitude/longitude
(:math:`\phi, \lambda`) and east/north cartesian coordinates (:math:`x^\prime, y^\prime`).
A simple 2-D rotation is used to convert east/north to grid coordinates (:math:`x, y`).

.. _PROJ.4: https://trac.osgeo.org/proj/

Forward Projection
~~~~~~~~~~~~~~~~~~

:math:`(\phi, \lambda) \rightarrow (x, y)`

1. Use *PROJ.4* to convert from (:math:`\phi, \lambda`) to (:math:`x^\prime, y^\prime`) using
   a Transverse Mercator projection, the WGS84 ellipsoid, central meridian :math:`\lambda_0`,
   central parallel :math:`\phi_0`, and cartesian units of U.S. yards.
2. Use the equations below to convert to grid coordinates:

.. math::

    x = x^{\prime}\cos{\alpha}-y^{\prime}\sin{\alpha}

.. math::

    y = x^{\prime}\sin{\alpha}+y^{\prime}\cos{\alpha}


Inverse Projection
~~~~~~~~~~~~~~~~~~

:math:`(x, y) \rightarrow (\phi, \lambda)`

1. Use the equations below to convert grid coordinates to east/north:

.. math::

    \alpha^\prime = 360 - \alpha

.. math::

    x^\prime = {x}\cos{\alpha^\prime}-{y}\sin{\alpha^\prime}

.. math::

    y^\prime = {x}\sin{\alpha^\prime}+{y}\cos{\alpha^\prime}

2. Use *PROJ.4* in inverse-mode to convert from (:math:`x^\prime, y^\prime`) to
   (:math:`\phi, \lambda`)  using
   a Transverse Mercator projection, the WGS84 ellipsoid, central meridian :math:`\lambda_0`,
   central parallel :math:`\phi_0`, and cartesian units of U.S. yards.


Range Survey Procedure
----------------------

This method is based on the acoustic survey method. RF-GPS unit *i-1*
should correspond to Hydrophone *i*. Hydrophone 1 must be in the "upper
left corner" of the grid with the numbers increasing as one moves
counter-clockwise around the grid.

This method is well-tested and allows for taking a long time average of
the ranges to reduce the noise in the GPS positions.

#. At each time-step, record the latitude and longitude of each Hydrophone
   :math:`(\phi_i, \lambda_i)` and the Origin GPS :math:`(\phi_0, \lambda_0)`.

#. Use the great_circle function from *PROJ.4* to calculate the range
   between every pair of Hydrophones and between the Origin GPS and every
   Hydrophone.  Store these values in a matrix :math:`R_{i,j}`. For
   example, The range from Hydrophone 2 to Hydrophone 3 will be stored in
   :math:`R_{2,3}`, the range from the Origin GPS to Hydrophone 3 will be
   stored in :math:`R_{0,3}`. Note that :math:`R_{i,j} = R_{j,i}`.

#. Average the range matrix over some suitable time interval (15-30 minutes).
   :math:`\rightarrow {R^\prime}_{i,j}`

#. Use the quadrilateral rule to calculate :math:`({x^\prime}_i,
   {y^\prime}_i)` from :math:`{R^\prime}_{i,j}` by placing the grid origin at
   Hydrophone 4 and letting the vector from Hydrophone 2 to Hydrophone 1 be
   the positive Y axis. Implemented in :func:`pyicex.survey.xyfit`.

#. Use the 3D spherical solution (trilateration_) with the ranges from
   each Hydrophone to the Origin GPS to calculate its X-Y position
   :math:`(x_0, y_0)`.  Translate each Hydrophone location (subtract
   :math:`(x_0, y_0)`) so this point becomes the new grid origin.
   Implemented in :func:`pyicex.survey.trilateration`.

#. Use the great_circle function from *PROJ.4* to calculate the true
   azimuth from Hydrophone 2 to Hydrophone 1, :math:`\alpha_{2,1}`. Rotate
   all of the Hydrophone coordinates by :math:`\alpha_{2,1}` to orient the
   Y-axis with true north. :math:`\rightarrow(x_i, y_i)`

#. Archive :math:`(x_i, y_i)` along with the derived values, range
   (:math:`r_i`) and bearing (:math:`\theta_i`)

.. math::

    \theta_i = 90 - \deg(\arctan{\frac{y_i}{x_i}})

.. math::

    r_i = \sqrt{x^2_i + y^2_i}

.. _trilateration: http://en.wikipedia.org/wiki/Trilateration


Range Check Procedure
---------------------

This calculation provides a continuous check of the range to each RF-GPS
unit and provides an updated value for the grid azimuth. This calculation
will be performed in real time and the output archived on a regular
interval.

Inputs
~~~~~~

  * UTC date/time
  * Origin GPS location (:math:`\phi_0, \lambda_0`)
  * RF-GPS locations (:math:`\phi_i, \lambda_i`)
  * Range Survey output (:math:`x_i, y_i`), (:math:`r_i, \theta_i`)

Outputs
~~~~~~~

  * UTC date/time
  * Grid Origin (:math:`\phi_0, \lambda_0`)
  * Grid azimuth (:math:`\alpha`)

Procedure
~~~~~~~~~

1. Use the PROJ.4 library to calculate the great-circle distance and
   bearing from the Origin GPS to each RF-GPS unit. :math:`\rightarrow
   (r^\prime_i, \theta^\prime_i)`

2. Calculate the absolute difference between the surveyed range and
   currently measured range (:math:`\lvert{r^\prime_i - r_i}\rvert`). If
   this value exceeds some specified threshold, raise an alarm.

.. note::

    A moving average filter will be applied to the range values before the
    threshold test to avoid spurious alarms.

3. The median of the differences between :math:`\theta^\prime_i` and
   :math:`\theta` is the new value value of :math:`\alpha`:

.. math::

    \alpha = Median((\theta^\prime_1 - \theta_1), (\theta^\prime_2 - \theta_2), \dotsc)
