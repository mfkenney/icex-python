.. pyicex documentation master file, created by
   sphinx-quickstart on Sat Jan  5 19:32:05 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================
ICEX Data Collection
====================

Contents:

.. toctree::
   :maxdepth: 4

   intro
   gps_data_collection
   met_data_collection
   data_storage
   programs
   pyicex

