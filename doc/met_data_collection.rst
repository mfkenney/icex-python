ICEX Meteorological Data Collection
===================================

The ICEX meteorological data is provided by a Weatherpack WP-2000 Met Station
manufactured by Coastal Environmental Systems. This device connects to an
RS-232 serial port on the data-collection system and is preconfigured to output
the following variables on a periodic basis:

* unit serial number
* sample date
* sample time
* average wind speed (m/s)
* average wind direction (degrees)
* wind gust (m/s)
* wind direction standard deviation (degrees)
* air temperature (deg C)
* barometric pressure (mb)
* relative humidity (%)
* battery voltage (V)
